#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>
#include <ctime>
#include <algorithm>

#include <random>

#include "max_cut_alg.h"



using namespace std;


uniform_int_distribution<int>my_unif_dist(15, 85);
normal_distribution<double>my_n_dist(0.5, 0.3);

#define WEIGHT_MODE 0
//0 uniform
//1 normal
//2 wieght=1

void druka_grafu(const vertice *g, const int vert_cnt){

    printf("\n");
    for(int i=0; i<vert_cnt; ++i){
        printf("%d:",i);
        for(edge e: g[i].edges){
            printf("\tv=%d w=%d\n", e.to_vertice, e.weight);
        }
        printf("\n");
    }
    printf("\n");

}//druka_grafu


int cur_weight(){


    int cur_w;

    #if WEIGHT_MODE==0
    cur_w = my_unif_dist(my_rand);
    #elif WEIGHT_MODE==1
    cur_w = round(my_n_dist(my_rand)*100);
    while(cur_w<0){cur_w = round(my_n_dist(my_rand)*100);}
    #else
    cur_w=1;
    #endif // w_mode


    return cur_w;
}//cur_weight


bool genere_3grafu_1(vertice *graph, int vert_cnt){

    //sadala 2 dalas, tad attiecigi savelk skautnes
    //sis ir stipri specifisks grafs!

    if(vert_cnt&1!=0 || vert_cnt<4){
        cerr<<"kluda, nav 2n skaits virsotnu!!"<<endl;
        return false;
    }

    int vdal2=vert_cnt/2;
    int cur_w;

    for(int i=0; i<vert_cnt; ++i)graph[i].edges.clear();


    for(int i=0; i<vdal2-1; ++i){
        cur_w=cur_weight();
        graph[i].edges.push_back(edge(i+1, cur_w));
        graph[i+1].edges.push_back(edge(i, cur_w));
    }
    cur_w=cur_weight();
    graph[0].edges.push_back(edge(vdal2-1,cur_w));
    graph[vdal2-1].edges.push_back(edge(0,cur_w));

    for(int i=vdal2; i<vert_cnt-1; ++i){
        cur_w=cur_weight();
        graph[i].edges.push_back(edge(i+1,cur_w));
        graph[i+1].edges.push_back(edge(i,cur_w));
    }
    cur_w=cur_weight();
    graph[vdal2].edges.push_back(edge(vert_cnt-1,cur_w));
    graph[vert_cnt-1].edges.push_back(edge(vdal2,cur_w));




    for(int i=0; i<vdal2; ++i){
        cur_w=cur_weight();
        graph[i].edges.push_back(edge(i+vdal2,cur_w));
        graph[i+vdal2].edges.push_back(edge(i,cur_w));
    }

//    for(int i=0; i<vert_cnt; ++i){
//        for(edge e: graph[i].edges)printf("%d ",e.to_vertice);
//        printf("\n");
//    }



    return true;
}

bool genere_m_grafu_2(vertice *graph, const int vert_cnt, const int deg){

    if((vert_cnt*deg)&1!=0 || deg>=vert_cnt){
        cerr<<"kluda, nav korekts skaits virsotnu!!"<<endl;
        return false;
    }


    int *cur_deg=new int[vert_cnt];

    int *pieejamas_v=new int[vert_cnt];
    int pieejamo_sk;
    int to_v;
    int cur_w;


    int N=10;
    int gen_reize=0;
    bool done=false;

    for(gen_reize=0; gen_reize<N&&!done; ++gen_reize){

        memset(cur_deg,0,4*vert_cnt);
        for(int i=0; i<vert_cnt; ++i){
            graph[i].edges.clear();
        }


        for(int i=0; i<vert_cnt; ++i){
            if(cur_deg[i]<deg){

                pieejamo_sk=0;
                done=true;
                for(int k=0; k<vert_cnt; ++k){
                    if(k<=i || cur_deg[k]==deg)continue;
                    pieejamas_v[pieejamo_sk++]=k;
                }

                if(pieejamo_sk==0){
                    done=false;
                    break;
                }


                int ind=rand()%pieejamo_sk;
                to_v=pieejamas_v[ind];
                ++cur_deg[i];
                ++cur_deg[to_v];

                cur_w=cur_weight();
                graph[i].edges.push_back(edge(to_v,cur_w));
                graph[to_v].edges.push_back(edge(i, cur_w));

                while(cur_deg[i]<deg){
                    for(int m=ind+1; m<pieejamo_sk; ++m)pieejamas_v[m-1]=pieejamas_v[m];
                    --pieejamo_sk;


                    if(pieejamo_sk==0){
                        done=false;
                        break;
                    }

                    ind=rand()%pieejamo_sk;

                    to_v=pieejamas_v[ind];

                    ++cur_deg[i];
                    ++cur_deg[to_v];

                    cur_w=cur_weight();
                    graph[i].edges.push_back(edge(to_v,cur_w));
                    graph[to_v].edges.push_back(edge(i, cur_w));

                }
                if(!done) break;
            }
        }


    }//liela for beigas






    return done;

}//genere_3grafu2




void genere_grafu_ar_max_deg(vertice *graph, const int vert_cnt, const int max_deg){




    int *cur_deg=new int[vert_cnt];

    int *pieejamas_v=new int[vert_cnt];
    int pieejamo_sk;
    int to_v;
    int cur_w;





    memset(cur_deg,0,4*vert_cnt);
    for(int i=0; i<vert_cnt; ++i){
        graph[i].edges.clear();
    }


    for(int i=0; i<vert_cnt; ++i){
        if(cur_deg[i]<max_deg){

            pieejamo_sk=0;
            for(int k=0; k<vert_cnt; ++k){
                if(k<=i || cur_deg[k]==max_deg)continue;
                pieejamas_v[pieejamo_sk++]=k;
            }

            if(pieejamo_sk==0){
                continue;
            }


            int ind=rand()%pieejamo_sk;
            to_v=pieejamas_v[ind];
            ++cur_deg[i];
            ++cur_deg[to_v];

            cur_w=cur_weight();
            graph[i].edges.push_back(edge(to_v,cur_w));
            graph[to_v].edges.push_back(edge(i, cur_w));

            while(cur_deg[i]<max_deg){
                for(int m=ind+1; m<pieejamo_sk; ++m)pieejamas_v[m-1]=pieejamas_v[m];
                --pieejamo_sk;


                if(pieejamo_sk==0){
                    break;
                }

                ind=rand()%pieejamo_sk;

                to_v=pieejamas_v[ind];

                ++cur_deg[i];
                ++cur_deg[to_v];

                cur_w=cur_weight();
                graph[i].edges.push_back(edge(to_v,cur_w));
                graph[to_v].edges.push_back(edge(i, cur_w));

            }
        }
    }


}//genere_grafu_ar_max_deg




int genere_random_grafu_2(vertice *graph, const int vert_cnt){

    bernoulli_distribution my_b_dist( 4.0/(vert_cnt));
    int cur_w;
    int e_cnt=0;
    for(int i=0; i<vert_cnt; ++i)graph[i].edges.clear();

    for(int i=0; i<vert_cnt-1; ++i){
        for(int j=i+1; j<vert_cnt; ++j){
            if(my_b_dist(my_rand)){
                cur_w=cur_weight();
                graph[i].edges.push_back(edge(j,cur_w));
                graph[j].edges.push_back(edge(i,cur_w));
                ++e_cnt;
            }
        }
    }
    return e_cnt;

}




void gen_orig(vertice *graph, int skaits){

    int max_rez, rez1,rez2,rez3;
    double proc1,proc2,proc3;

    int vert_cnt;

    int weights[21][21];
    int cnt_edges=0;

    fstream fin("orig_graph.txt",ios::in);
    int u,v;
    int cur_w;

    char fname[40]="";
    FILE *file_out;




    fin>>vert_cnt;
    while(fin>>u>>v>>cur_w){
        graph[u].edges.push_back(edge(v,cur_w));
        graph[v].edges.push_back(edge(u,cur_w));

        weights[u][v]=cur_w;
        weights[v][u]=cur_w;
        cnt_edges++;
    }
    fin.close();


    for(int grafa_nr=0; grafa_nr<skaits; ++grafa_nr){
        #if WEIGHT_MODE==0
        sprintf(fname,"orig_graph_unif_%d.txt",grafa_nr);
        #elif WEIGHT_MODE==1
        sprintf(fname,"orig_graph_norm_%d.txt",grafa_nr);
        #else
        sprintf(fname,"orig_graph_unif_%d.txt",grafa_nr);
        #endif // WEIGHT_MODE
        file_out=fopen(fname,"w");

        for(int v=0; v<vert_cnt; ++v){
            for(auto it = graph[v].edges.begin(); it!=graph[v].edges.end(); ++it){
                if(v<(*it).to_vertice){
                    cur_w=cur_weight();

                    weights[v][(*it).to_vertice]=cur_w;
                    weights[(*it).to_vertice][v]=cur_w;

                    (*it).weight=cur_w;

                    //printf("%d ",cur_w);
                }
                else{
                    (*it).weight = weights[(*it).to_vertice][v];
                }
            }
        }

        fprintf(file_out,"%d\n",vert_cnt);
        for(int i=0; i<vert_cnt; ++i){
            for(edge e:graph[i].edges){
                if(i<e.to_vertice)fprintf(file_out,"%d %d %d\n",i,e.to_vertice,e.weight);
            }
        }

        fclose(file_out);
    }

}//gen_orig




void genere_3dal_grafu(vertice *graph, int vert_cnt){
    bernoulli_distribution my_b_dist(0.5);
    int w;

    vector<int>perm(vert_cnt);

    for(int i=0; i<vert_cnt; ++i){
        graph[i].edges.clear();
        perm[i]=i;
    }



    for(int i=0; i<vert_cnt; ++i){
        for(int j=i+1; j<vert_cnt; ++j){
            if(my_b_dist(my_rand)){
                swap(perm[i],perm[j]);
            }
        }
    }


    for(int i=0; i<vert_cnt/3; ++i){
        for(int j=vert_cnt/3; j<vert_cnt; ++j){
            if(my_b_dist(my_rand)){
                w=cur_weight();
                graph[perm[i]].edges.push_back(edge(perm[j],w));
                graph[perm[j]].edges.push_back(edge(perm[i],w));
            }
        }
    }



    for(int i=vert_cnt/3; i<(2*vert_cnt)/3; ++i){
        for(int j=(2*vert_cnt)/3; j<vert_cnt; ++j){
            if(my_b_dist(my_rand)){
                w=cur_weight();
                graph[perm[i]].edges.push_back(edge(perm[j],w));
                graph[perm[j]].edges.push_back(edge(perm[i],w));
            }
        }
    }










}//gen_3dal_grafu




pair<int,int> genere_random_grafu_1(vertice *graph, int vert_cnt, const double p1, const double p2){

    bernoulli_distribution dist_init(0.5);
    bernoulli_distribution dist1(p1);
    bernoulli_distribution dist2(p2);

    int cnt_edges=0;
    int tuvais_cut=0;



    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=dist_init(my_rand);
        graph[i].edges.clear();
    }


    for(int i=0; i<vert_cnt-1; ++i){
        for(int j=i+1; j<vert_cnt; ++j){
            if(graph[i].cut_val!=graph[j].cut_val){
                if(dist1(my_rand)){
                    graph[i].edges.push_back(edge(j));
                    graph[j].edges.push_back(edge(i));
                    ++cnt_edges;
                    ++tuvais_cut;
                }
            }
            else{
                if(dist2(my_rand)){
                    graph[i].edges.push_back(edge(j));
                    graph[j].edges.push_back(edge(i));
                    ++cnt_edges;
                }
            }
        }
    }


    //printf("Uzgenerets grafs ar %d skautnem   tuvais cut=%d\n\n",cnt_edges,tuvais_cut);

    return make_pair(cnt_edges, tuvais_cut);


}//genere_random_grafu_2



void genere_Kn(vertice *graph, int vert_cnt){
    int cur_w;
    for(int i=0; i<vert_cnt; ++i){
        graph[i].edges.clear();
        for(int j=i+1; j<vert_cnt; ++j){
            cur_w = cur_weight();
            graph[i].edges.push_back(edge(j,cur_w));
            graph[j].edges.push_back(edge(i,cur_w));
        }
    }
}//gen_Kn



void gen_dazadi(vertice *graph, int vert_cnt, int grafu_skaits, int reg=3){

    char fname[200];
    FILE *f;

    for(int g=0; g<grafu_skaits; ++g){

        //sprintf(fname,"reg%d_%d_%d.txt",reg, vert_cnt, g); //regulariem
        //sprintf(fname,"rand1_%d_%d.txt", vert_cnt, g); //random1
        //sprintf(fname,"rand2_%d_%d.txt", vert_cnt, g); //random2
        //sprintf(fname,"3dal_%d_%d.txt",vert_cnt, g); //3daligiem
        //sprintf(fname,"K_%d_%d.txt",vert_cnt, g); //pilniem



        f=fopen(fname,"w");

        fprintf(f,"%d\n",vert_cnt);

        //while(!genere_m_grafu_2(graph, vert_cnt,reg)){}
        //genere_random_grafu_1(graph, vert_cnt, 0.6, 0.2);
        //genere_random_grafu_2(graph, vert_cnt);
        //genere_3dal_grafu(graph, vert_cnt);
        //genere_Kn(graph,vert_cnt);

        for(int i=0; i<vert_cnt; ++i){
            for(edge e: graph[i].edges){
                if(i<e.to_vertice){
                    fprintf(f,"%d %d %d\n",i, e.to_vertice, e.weight);
                }
            }
        }

        fclose(f);
    }



}//gen_dazadi




void druka_adj_M(vertice *graph, int vert_cnt){
    vector<vector<int>>M;
    M.resize(vert_cnt,vector<int>(vert_cnt,0));

    for(int i=0; i<vert_cnt; ++i){
        for(edge e:graph[i].edges){
            M[i][e.to_vertice]=e.weight;
            M[e.to_vertice][i]=e.weight;
        }
    }

    for(row:M){
        for(el:row){
            printf("%d ",el);
        }
        printf("\n");
    }


}




int main()
{
    const int max_vert_cnt=1000000;
    vertice *graph;
    int vert_cnt;
//    int u,v;
//    int cur_w;
//
//    int tmp_rez, rez;


    graph=new vertice[max_vert_cnt];


    //gen_orig(graph, 10);

    vert_cnt=50;
    int gr_sk=100;

    //gen_dazadi(graph, vert_cnt, gr_sk,3);//reg3
    //gen_dazadi(graph, vert_cnt, gr_sk,4);//reg4


    //gen_dazadi(graph, vert_cnt, gr_sk);//parejie












    return 0;
}//main
