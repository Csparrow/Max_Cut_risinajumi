#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>

#include <random>

#include  "max_cut_alg.h"

using namespace std;


mt19937 my_rand;



int random_Max_cut_1(vertice *graph, const int vert_cnt, const int it_sk){

    int rez=0;
    int cur_rez;
    //mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    edge e;

    //my_rand.seed(rand());


    for(int it=0; it<it_sk; ++it){
        cur_rez=0;
        for(int i=0; i<vert_cnt; ++i){
            //cut[i]=my_dist(my_rand);
            //printf("%d ",cut[i]);
            graph[i].cut_val = my_dist(my_rand);
        }
        //printf("  ");


        for(int i=0; i<vert_cnt; ++i){
            for(int j=0; j<graph[i].edges.size(); ++j){
                e=graph[i].edges[j];
                if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                    cur_rez+=e.weight;
                }
            }
        }
        //printf("cur rez:%.2f\n",rez);
        rez=max(rez,cur_rez);

    }//liela for beigas


    return rez;

}//random_Max_cut_1









int mans_Max_cut_v_parcelajs(vertice *graph, const int vert_cnt){
    int rez=0;

    int cur_diff, best_diff;
    pair<int,bool>*best_v=new pair<int,bool>[vert_cnt];

    int *diff=new int[vert_cnt];

    int len;

    int chosen_vert_ind;
    bool cur_cut_side;

    uniform_int_distribution<int> my_dist;



    for(int i=0; i<vert_cnt; ++i){
        diff[i]=0;
        for(edge e: graph[i].edges){
            if(!graph[e.to_vertice].cut_val)diff[i]+=e.weight;
            else diff[i]-=e.weight;

            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez+=e.weight;
            }
        }
    }




    while(true){

        best_diff=0;
        len=0;
        ///atrod 'max'
        for(int i=0; i<vert_cnt; ++i){
            cur_cut_side=graph[i].cut_val;
            if(!cur_cut_side)cur_diff=diff[i];
            else cur_diff=-diff[i];

            if(cur_diff>best_diff){
                len=1;
                best_v[0]=pair<int,bool>(i,cur_cut_side);
                best_diff=cur_diff;
            }
            else if(cur_diff==best_diff)best_v[len++]=pair<int,bool>(i,cur_cut_side);
        }


        if(best_diff>0){
            rez+=best_diff;
            if(len==1){
                chosen_vert_ind = best_v[0].first;
                cur_cut_side = best_v[0].second;
            }
            else{
                int temp_ind=my_dist(my_rand)%len;
                chosen_vert_ind = best_v[temp_ind].first;
                cur_cut_side = best_v[temp_ind].second;
            }





            for(edge e: graph[chosen_vert_ind].edges){
                if(!graph[chosen_vert_ind].cut_val){
                    diff[e.to_vertice]-=2*e.weight;
                }
                else{
                    diff[e.to_vertice]+=2*e.weight;
                }
            }
            graph[chosen_vert_ind].cut_val=!cur_cut_side;

        }
        else break;


    }




//    for(int i=0; i<vert_cnt; ++i){
//        for(edge e: graph[i].edges){
//            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
//                rez+=e.weight;
//            }
//        }
//    }



    delete []best_v;
    delete[]diff;

    return rez;
}//mans_Max_cut_v_parcelajs






int mans_Max_cut_random_v_parcelajs(vertice *graph, const int vert_cnt){
    int rez=0;

    int cur_diff;
    pair<int,bool>*best_v=new pair<int,bool>[vert_cnt];

    int *diff=new int[vert_cnt];

    int len;

    int chosen_vert_ind;
    bool cur_cut_side;

    uniform_int_distribution<int> my_dist;



    for(int i=0; i<vert_cnt; ++i){
        diff[i]=0;
        for(edge e: graph[i].edges){
            if(!graph[e.to_vertice].cut_val)diff[i]+=e.weight;
            else diff[i]-=e.weight;

            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez+=e.weight;
            }
        }
    }




    while(true){
        len=0;
        for(int i=0; i<vert_cnt; ++i){
            cur_cut_side=graph[i].cut_val;
            if(!cur_cut_side)cur_diff=diff[i];
            else cur_diff=-diff[i];

            if(cur_diff>0){
                best_v[len++]=pair<int,bool>(i,cur_cut_side);
            }
        }


        if(len>0){
            int temp_ind=my_dist(my_rand)%len;
            chosen_vert_ind = best_v[temp_ind].first;
            cur_cut_side = best_v[temp_ind].second;

            if(!cur_cut_side)rez+=diff[chosen_vert_ind];
            else cur_diff-=diff[chosen_vert_ind];




            for(edge e: graph[chosen_vert_ind].edges){
                if(!graph[chosen_vert_ind].cut_val){
                    diff[e.to_vertice]-=2*e.weight;
                }
                else{
                    diff[e.to_vertice]+=2*e.weight;
                }
            }
            graph[chosen_vert_ind].cut_val=!cur_cut_side;

        }
        else break;


    }




//    for(int i=0; i<vert_cnt; ++i){
//        for(edge e: graph[i].edges){
//            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
//                rez+=e.weight;
//            }
//        }
//    }



    delete []best_v;
    delete[]diff;

    return rez;
}//mans_Max_cut_random_v_parcelajs









int mans_max_cut_1var(vertice *graph, const int vert_cnt){
    /// sakuma visas virsotnes kreisaja kopa - tad parcel virsotnes, lidz vairak neuzlabojas
    int rez;

    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=0;
    }

    rez = mans_Max_cut_v_parcelajs(graph, vert_cnt);

    return rez;

}//mans_max_cut_1var




int mans_max_cut_2var(vertice *graph, const int vert_cnt){
    /// sakuma visas virsotnes izmetaa randomaa - tad parcel virsotnes, lidz vairak neuzlabojas
    int rez;
    bernoulli_distribution my_dist(0.5);

    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=my_dist(my_rand);
    }

    rez = mans_Max_cut_v_parcelajs(graph, vert_cnt);

    return rez;

}//mans_max_cut_2var




int mans_max_cut_2_2var(vertice *graph, const int vert_cnt, const int it_sk){
    /// sakuma visas virsotnes izmetaa randomaa - tad parcel virsotnes, lidz vairak neuzlabojas
    /// atkarto it_sk reizes - panem labako
    int rez=0;
    int cur_rez;
    bernoulli_distribution my_dist(0.5);

    for(int reize=0; reize<it_sk; ++reize){

        for(int i=0; i<vert_cnt; ++i){
            graph[i].cut_val=my_dist(my_rand);
        }
        cur_rez = mans_Max_cut_v_parcelajs(graph, vert_cnt);

        if(cur_rez>rez)rez=cur_rez;
    }



    return rez;

}//mans_max_cut_2_2var






void DFS_liek_2dalas(vertice *graph, bool visited[], int cur_v, const bool cur_cut_val){

    visited[cur_v]=true;
    graph[cur_v].cut_val=cur_cut_val;

    for(edge e: graph[cur_v].edges){
        if(!visited[e.to_vertice]){
            DFS_liek_2dalas(graph, visited, e.to_vertice, !cur_cut_val);
        }
    }

}//DFS_liek_2dalas


int mans_max_cut_3var(vertice *graph, const int vert_cnt){
    /// sakuma visas virsotnes izmetaa meginot salikt ka 2daligu grafu - tad parcel virsotnes, lidz vairak neuzlabojas
    int rez=0;
    bool *visited=new bool[vert_cnt];

    memset(visited, false, vert_cnt);

    for(int i=0; i<vert_cnt; ++i){
        if(!visited[i]){
            DFS_liek_2dalas(graph, visited, i, false);
        }
    }

    rez = mans_Max_cut_v_parcelajs(graph, vert_cnt);




    delete[] visited;

    return rez;
}//mans_max_cut_3var









int pilna_parlase_Max_cut(vertice *graph, const int vert_cnt){

    int best_rez=0;
    int cur_rez;

    if(vert_cnt>25){
        printf("\n\nVirsotnu skaits:%d par lielu, lai veiktu pilno parlasi!!\n", vert_cnt);
        return -1;
    }

    int var_cnt=(1<<vert_cnt);

    for(int cur_var=0, temp; cur_var<var_cnt; ++cur_var){
        temp=cur_var;
        cur_rez=0;
        for(int i=vert_cnt-1; i>=0; --i){
            graph[i].cut_val = (bool)(temp&1);
            temp>>=1;
        }

        for(int i=0; i<vert_cnt; ++i){
            for(edge e: graph[i].edges){
                if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                    cur_rez+=e.weight;
                }
            }
        }
        if(cur_rez>best_rez)best_rez=cur_rez;
    }

    return best_rez;
}//pilna_parlase_Max_cut

