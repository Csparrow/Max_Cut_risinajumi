#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>
#include <ctime>

#include <random>

#include "max_cut_alg.h"



using namespace std;


uniform_int_distribution<int>my_unif_dist(15, 85);
normal_distribution<double>my_n_dist(0.5, 0.3);

#define WEIGHT_MODE 2
//0 uniform
//1 normal
//2 wieght=1

void druka_grafu(const vertice *g, const int vert_cnt){

    printf("\n");
    for(int i=0; i<vert_cnt; ++i){
        printf("%d:",i);
        for(edge e: g[i].edges){
            printf("\tv=%d w=%d\n", e.to_vertice, e.weight);
        }
        printf("\n");
    }
    printf("\n");

}//druka_grafu


int cur_weight(){


    int cur_w;

    #if WEIGHT_MODE==0
    cur_w = my_unif_dist(my_rand);
    #elif WEIGHT_MODE==1
    cur_w = round(my_n_dist(my_rand)*100);
    while(cur_w<0){cur_w = round(my_n_dist(my_rand)*100);}
    #else
    cur_w=1;
    #endif // w_mode


    return cur_w;
}//cur_weight


bool genere_3grafu_1(vertice *graph, int vert_cnt){

    //sadala 2 dalas, tad attiecigi savelk skautnes
    //sis ir stipri specifisks grafs!

    if(vert_cnt&1!=0 || vert_cnt<4){
        cerr<<"kluda, nav 2n skaits virsotnu!!"<<endl;
        return false;
    }

    int vdal2=vert_cnt/2;
    int cur_w;

    for(int i=0; i<vert_cnt; ++i)graph[i].edges.clear();


    for(int i=0; i<vdal2-1; ++i){
        cur_w=cur_weight();
        graph[i].edges.push_back(edge(i+1, cur_w));
        graph[i+1].edges.push_back(edge(i, cur_w));
    }
    cur_w=cur_weight();
    graph[0].edges.push_back(edge(vdal2-1,cur_w));
    graph[vdal2-1].edges.push_back(edge(0,cur_w));

    for(int i=vdal2; i<vert_cnt-1; ++i){
        cur_w=cur_weight();
        graph[i].edges.push_back(edge(i+1,cur_w));
        graph[i+1].edges.push_back(edge(i,cur_w));
    }
    cur_w=cur_weight();
    graph[vdal2].edges.push_back(edge(vert_cnt-1,cur_w));
    graph[vert_cnt-1].edges.push_back(edge(vdal2,cur_w));




    for(int i=0; i<vdal2; ++i){
        cur_w=cur_weight();
        graph[i].edges.push_back(edge(i+vdal2,cur_w));
        graph[i+vdal2].edges.push_back(edge(i,cur_w));
    }

//    for(int i=0; i<vert_cnt; ++i){
//        for(edge e: graph[i].edges)printf("%d ",e.to_vertice);
//        printf("\n");
//    }



    return true;
}

bool genere_m_grafu_2(vertice *graph, const int vert_cnt, const int deg){

    if((vert_cnt*deg)&1!=0 || deg>=vert_cnt){
        cerr<<"kluda, nav korekts skaits virsotnu!!"<<endl;
        return false;
    }


    int *cur_deg=new int[vert_cnt];

    int *pieejamas_v=new int[vert_cnt];
    int pieejamo_sk;
    int to_v;
    int cur_w;


    int N=10;
    int gen_reize=0;
    bool done=false;

    for(gen_reize=0; gen_reize<N&&!done; ++gen_reize){

        memset(cur_deg,0,4*vert_cnt);
        for(int i=0; i<vert_cnt; ++i){
            graph[i].edges.clear();
        }


        for(int i=0; i<vert_cnt; ++i){
            if(cur_deg[i]<deg){

                pieejamo_sk=0;
                done=true;
                for(int k=0; k<vert_cnt; ++k){
                    if(k<=i || cur_deg[k]==deg)continue;
                    pieejamas_v[pieejamo_sk++]=k;
                }

                if(pieejamo_sk==0){
                    done=false;
                    break;
                }


                int ind=rand()%pieejamo_sk;
                to_v=pieejamas_v[ind];
                ++cur_deg[i];
                ++cur_deg[to_v];

                cur_w=cur_weight();
                graph[i].edges.push_back(edge(to_v,cur_w));
                graph[to_v].edges.push_back(edge(i, cur_w));

                while(cur_deg[i]<deg){
                    for(int m=ind+1; m<pieejamo_sk; ++m)pieejamas_v[m-1]=pieejamas_v[m];
                    --pieejamo_sk;


                    if(pieejamo_sk==0){
                        done=false;
                        break;
                    }

                    ind=rand()%pieejamo_sk;

                    to_v=pieejamas_v[ind];

                    ++cur_deg[i];
                    ++cur_deg[to_v];

                    cur_w=cur_weight();
                    graph[i].edges.push_back(edge(to_v,cur_w));
                    graph[to_v].edges.push_back(edge(i, cur_w));

                }
                if(!done) break;
            }
        }


    }//liela for beigas






    return done;

}//genere_3grafu2


/*
bool genere_3grafu(vertice *graph, int virs_sk){

    if(virs_sk&1!=0 || virs_sk<4){
        cerr<<"kluda, nav para skaits virsotnu!!"<<endl;
        return false;
    }


    int *cur_deg=new int[virs_sk];

    int *pieejamas_v=new int[virs_sk];
    int pieejamo_sk;
    //uniform_int_distribution<int>my_unif_dist(0, virs_sk-1);
    int to_v;

    memset(cur_deg,0,4*virs_sk);
    for(int i=0; i<virs_sk; ++i){
        graph[i].edges.clear();
    }


    for(int i=0; i<virs_sk; ++i){
        if(cur_deg[i]<3){

            pieejamo_sk=0;
            for(int k=0; k<virs_sk; ++k){
                if(k<=i || cur_deg[k]==3)continue;
                pieejamas_v[pieejamo_sk++]=k;
            }


            int ind=rand()%pieejamo_sk;
            to_v=pieejamas_v[ind];
            ++cur_deg[i];
            ++cur_deg[to_v];
            graph[i].edges.push_back(edge(to_v));
            graph[to_v].edges.push_back(edge(i));

            while(cur_deg[i]<3){
                for(int m=ind+1; m<pieejamo_sk; ++m)pieejamas_v[m-1]=pieejamas_v[m];
                --pieejamo_sk;



                ind=rand()%pieejamo_sk;

                to_v=pieejamas_v[ind];

                ++cur_deg[i];
                ++cur_deg[to_v];
                graph[i].edges.push_back(edge(to_v));
                graph[to_v].edges.push_back(edge(i));

            }

        }

        for(int i=0; i<virs_sk; ++i)cout<<cur_deg[i]<<" ";
        cout<<endl;

    }


    for(int i=0; i<virs_sk; ++i){
        for(edge e: graph[i].edges)printf("%d ",e.to_vertice);
        printf("\n");
    }


    return true;

}//genere_3grafu
*/



void genere_grafu_ar_max_deg(vertice *graph, const int vert_cnt, const int max_deg){




    int *cur_deg=new int[vert_cnt];

    int *pieejamas_v=new int[vert_cnt];
    int pieejamo_sk;
    int to_v;
    int cur_w;





    memset(cur_deg,0,4*vert_cnt);
    for(int i=0; i<vert_cnt; ++i){
        graph[i].edges.clear();
    }


    for(int i=0; i<vert_cnt; ++i){
        if(cur_deg[i]<max_deg){

            pieejamo_sk=0;
            for(int k=0; k<vert_cnt; ++k){
                if(k<=i || cur_deg[k]==max_deg)continue;
                pieejamas_v[pieejamo_sk++]=k;
            }

            if(pieejamo_sk==0){
                continue;
            }


            int ind=rand()%pieejamo_sk;
            to_v=pieejamas_v[ind];
            ++cur_deg[i];
            ++cur_deg[to_v];

            cur_w=cur_weight();
            graph[i].edges.push_back(edge(to_v,cur_w));
            graph[to_v].edges.push_back(edge(i, cur_w));

            while(cur_deg[i]<max_deg){
                for(int m=ind+1; m<pieejamo_sk; ++m)pieejamas_v[m-1]=pieejamas_v[m];
                --pieejamo_sk;


                if(pieejamo_sk==0){
                    break;
                }

                ind=rand()%pieejamo_sk;

                to_v=pieejamas_v[ind];

                ++cur_deg[i];
                ++cur_deg[to_v];

                cur_w=cur_weight();
                graph[i].edges.push_back(edge(to_v,cur_w));
                graph[to_v].edges.push_back(edge(i, cur_w));

            }
        }
    }


}//genere_grafu_ar_max_deg




int genere_random_grafu_2(vertice *graph, const int vert_cnt){

    bernoulli_distribution my_b_dist( 4.0/(vert_cnt));
    int cur_w;
    int e_cnt=0;
    for(int i=0; i<vert_cnt; ++i)graph[i].edges.clear();

    for(int i=0; i<vert_cnt-1; ++i){
        for(int j=i+1; j<vert_cnt; ++j){
            if(my_b_dist(my_rand)){
                cur_w=cur_weight();
                graph[i].edges.push_back(edge(j,cur_w));
                graph[j].edges.push_back(edge(i,cur_w));
                ++e_cnt;
            }
        }
    }
    return e_cnt;

}




void analize1(vertice *graph){

    int max_rez, rez1,rez2,rez3;
    double proc1,proc2,proc3;

    int vert_cnt;

    int weights[21][21];
    int cnt_edges=0;

    fstream fin("in.txt",ios::in);
    int u,v;
    int cur_w;


    FILE *file_out=fopen("analizes1_rez.txt","w");
    my_unif_dist.reset();
    my_n_dist.reset();


    int N=10;


    #if WEIGHT_MODE==0
    fprintf(file_out, "Analize 1 ar vienmerigo sadalijumu\n");
    #else
    fprintf(file_out, "Analize 1 ar normalo sadalijumu\n");
    #endif



    fprintf(file_out, "Grafu skaits: %d +1\n", N);


    //fprintf(file_out, "Nr       rez1%%     rez2%%     rez3%%    max_cut\n");
    fprintf(file_out, "Nr       rez1%%     rez2%%    max_cut\n");


    fin>>vert_cnt;
    while(fin>>u>>v>>cur_w){
        graph[u].edges.push_back(edge(v,cur_w));
        graph[v].edges.push_back(edge(u,cur_w));

        weights[u][v]=cur_w;
        weights[v][u]=cur_w;
        cnt_edges++;
    }
    fin.close();



    rez1=mans_max_cut_1var(graph, vert_cnt);
    rez2=mans_max_cut_2_2var(graph, vert_cnt,10);
    max_rez=mans_max_cut_3var(graph, vert_cnt);
    //rez2=mans_max_cut_2var(graph, vert_cnt);
    //rez3=mans_max_cut_3var(graph, vert_cnt);
    //max_rez=pilna_parlase_Max_cut(graph, vert_cnt);

    proc1 = ((double)(rez1)/(double)(max_rez))*100;
    proc2 = ((double)(rez2)/(double)(max_rez))*100;
    //proc3 = ((double)(rez3)/(double)(max_rez))*100;



    //fprintf(file_out," * %11.2f%10.2f%10.2f %10d\n", proc1, proc2, proc3, max_rez);
    fprintf(file_out,"* %11.2f%10.2f %10d\n", proc1, proc2, max_rez);



    for(int i=1; i<=N; ++i){

        //printf("%3d/%3d  ... ", i, N);

        for(int v=0; v<vert_cnt; ++v){
            for(auto it = graph[v].edges.begin(); it!=graph[v].edges.end(); ++it){
                if(v<(*it).to_vertice){
                    cur_w=cur_weight();

                    weights[v][(*it).to_vertice]=cur_w;
                    weights[(*it).to_vertice][v]=cur_w;

                    (*it).weight=cur_w;

                    //printf("%d ",cur_w);
                }
                else{
                    (*it).weight = weights[(*it).to_vertice][v];
                }
            }
        }
        //printf("\n\n");

        rez1=mans_max_cut_1var(graph, vert_cnt);
        rez2=mans_max_cut_2_2var(graph, vert_cnt,10);
        max_rez=mans_max_cut_3var(graph, vert_cnt);
        //rez2=mans_max_cut_2var(graph, vert_cnt);
        //rez3=mans_max_cut_3var(graph, vert_cnt);
        //max_rez=pilna_parlase_Max_cut(graph, vert_cnt);

        proc1 = ((double)(rez1)/(double)(max_rez))*100;
        proc2 = ((double)(rez2)/(double)(max_rez))*100;
        //proc3 = ((double)(rez3)/(double)(max_rez))*100;


        //fprintf(file_out,"%2d %11.2f%10.2f%10.2f %10d\n",i, proc1, proc2, proc3, max_rez);
        fprintf(file_out,"%d %11.2f%10.2f %10d\n",i, proc1, proc2, max_rez);

        //printf("\t[OK]\n");

    }

    fclose(file_out);

    //system("notepad E://LU_DF/Sem_6/Kursa_Darbs/Prog_Max_Cut/Max_cut_03/analizes1_rez.txt");

}//analize1




void analize2(vertice *graph){

    /// analize2 prieksh 3grafiem un 4 grafiem.

    FILE *file_out=fopen("analizes2_rez.txt","w");

    int vert_cnt=1000;
    int rez;
    const int N=200;//10 1000 200 10
    int cur_grafa_nr;
    //const int m=4;

    #if WEIGHT_MODE==0
    fprintf(file_out, "Analize 2 ar vienmerigo sadalijumu\n");
    #elif WEIGHT_MODE==1
    fprintf(file_out, "Analize 2 ar normalo sadalijumu\n");
    #else
    fprintf(file_out, "Analize 2 nesverts grafs\n");
    #endif

    fprintf(file_out, "Grafu skaits: %d\n", N);
    fprintf(file_out,"v_skaits:%d  ",vert_cnt);




    fprintf(file_out,"rez_parastais rez_DFS rez_mans_random2 pilnigi_random random_parcelajs\n\n");

    cur_grafa_nr=0;
    while(cur_grafa_nr<N){
        //if(!genere_3grafu_1(graph, vert_cnt))continue;
        //if(!genere_m_grafu_2(graph, vert_cnt, 4))continue;
        //genere_grafu_ar_max_deg(graph, vert_cnt, 4);

        int asd=genere_random_grafu_2(graph, vert_cnt);



        fprintf(file_out,"%d ", mans_max_cut_1var(graph,vert_cnt));

        fprintf(file_out,"%6d ", mans_max_cut_3var(graph, vert_cnt));

        fprintf(file_out,"%6d ", mans_max_cut_2_2var(graph,vert_cnt, 1));//10

        fprintf(file_out,"%6d ", random_Max_cut_1(graph,vert_cnt, 1));//1000

        fprintf(file_out,"%6d ", mans_Max_cut_random_v_parcelajs(graph, vert_cnt));

        //fprintf(file_out,"%6d ", pilna_parlase_Max_cut(graph, vert_cnt));
        fprintf(file_out,"%6d",asd);

        fprintf(file_out,"\n");

        ++cur_grafa_nr;
    }




    fclose(file_out);
    //system("notepad E://LU_DF/Sem_6/Kursa_Darbs/Prog_Max_Cut/Max_cut_03/analizes2_rez.txt");
}//analize2



int main()
{
    const int max_vert_cnt=1000000;
    vertice *graph;
    int vert_cnt;
//    int u,v;
//    int cur_w;
//
//    int tmp_rez, rez;





    graph=new vertice[max_vert_cnt];


    //analize1(graph);
    analize2(graph);



    return 0;
}//main
