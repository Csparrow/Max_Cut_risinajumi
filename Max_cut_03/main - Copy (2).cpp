#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>

#include <random>

using namespace std;



mt19937 my_rand;


struct edge{
    int to_vertice;
    int weight;
    edge(int v,int w){
        to_vertice=v;
        weight=w;
    }
    edge(int v){
        to_vertice=v;
        weight=0;
    }
    edge(){
        weight=0;
    }
};


struct vertice{
    vector<edge>edges;
    //int diff_w;
    bool cut_val;
};




int random_Max_cut_1(vertice *graph, const int vert_cnt){

    int rez=0;
    //mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    edge e;

    //my_rand.seed(rand());


    for(int i=0; i<vert_cnt; ++i){
        //cut[i]=my_dist(my_rand);
        //printf("%d ",cut[i]);
        graph[i].cut_val = my_dist(my_rand);
    }
    //printf("  ");


    for(int i=0; i<vert_cnt; ++i){
        for(int j=0; j<graph[i].edges.size(); ++j){
            e=graph[i].edges[j];
            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez+=e.weight;
            }
        }
    }
    //printf("cur rez:%.2f\n",rez);



    return rez;

}//random_Max_cut_1









int mans_Max_cut_v_parcelajs(vertice *graph, const int vert_cnt){
    int rez=0;


    int cur_diff, best_diff;
    pair<int,bool>*best_v=new pair<int,bool>[vert_cnt];

    int *diff=new int[vert_cnt];

    int len;

    int chosen_vert_ind;
    bool cur_cut_side;

    uniform_int_distribution<int> my_dist;



    for(int i=0; i<vert_cnt; ++i){
        diff[i]=0;
        for(edge e: graph[i].edges){
            if(!graph[e.to_vertice].cut_val)diff[i]+=e.weight;
            else diff[i]-=e.weight;
        }
    }




    while(true){

        best_diff=0;
        len=0;
        ///atrod 'max'
        for(int i=0; i<vert_cnt; ++i){
            cur_cut_side=graph[i].cut_val;
            if(!cur_cut_side)cur_diff=diff[i];
            else cur_diff=-diff[i];

            if(cur_diff>best_diff){
                len=1;
                best_v[0]=pair<int,bool>(i,cur_cut_side);
                best_diff=cur_diff;
            }
            else if(cur_diff==best_diff)best_v[len++]=pair<int,bool>(i,cur_cut_side);
        }


        if(best_diff>0){
            if(len==1){
                chosen_vert_ind = best_v[0].first;
                cur_cut_side = best_v[0].second;
            }
            else{
                int temp_ind=my_dist(my_rand)%len;
                chosen_vert_ind = best_v[temp_ind].first;
                cur_cut_side = best_v[temp_ind].second;
            }





            for(edge e: graph[chosen_vert_ind].edges){
                if(!graph[chosen_vert_ind].cut_val){
                    diff[e.to_vertice]-=2*e.weight;
                }
                else{
                    diff[e.to_vertice]+=2*e.weight;
                }
            }
            graph[chosen_vert_ind].cut_val=!cur_cut_side;

        }
        else break;


    }




    for(int i=0; i<vert_cnt; ++i){
        for(edge e: graph[i].edges){
            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez+=e.weight;
            }
        }
    }



    delete []best_v;
    delete[]diff;

    return rez;
}//mans_Max_cut_v_parcelajs





int mans_max_cut_1var(vertice *graph, const int vert_cnt){
    /// sakuma visas virsotnes kreisaja kopa - tad parcel virsotnes, lidz vairak neuzlabojas
    int rez;

    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=0;
    }

    rez = mans_Max_cut_v_parcelajs(graph, vert_cnt);

    return rez;

}//mans_max_cut_1var




int mans_max_cut_2var(vertice *graph, const int vert_cnt){
    /// sakuma visas virsotnes izmetaa randomaa - tad parcel virsotnes, lidz vairak neuzlabojas
    int rez;
    bernoulli_distribution my_dist(0.5);

    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=my_dist(my_rand);
    }

    rez = mans_Max_cut_v_parcelajs(graph, vert_cnt);

    return rez;

}//mans_max_cut_2var




int mans_max_cut_2_2var(vertice *graph, const int vert_cnt, const int it_sk=10){
    /// sakuma visas virsotnes izmetaa randomaa - tad parcel virsotnes, lidz vairak neuzlabojas
    int rez=0;
    int cur_rez;
    bernoulli_distribution my_dist(0.5);

    for(int reize=0; reize<it_sk; ++reize){

        for(int i=0; i<vert_cnt; ++i){
            graph[i].cut_val=my_dist(my_rand);
        }
        cur_rez = mans_Max_cut_v_parcelajs(graph, vert_cnt);

        if(cur_rez>rez)rez=cur_rez;
    }



    return rez;

}//mans_max_cut_2_2var






void DFS_liek_2dalas(vertice *graph, bool visited[], int cur_v, const bool cur_cut_val){

    visited[cur_v]=true;
    graph[cur_v].cut_val=cur_cut_val;

    for(edge e: graph[cur_v].edges){
        if(!visited[e.to_vertice]){
            DFS_liek_2dalas(graph, visited, e.to_vertice, !cur_cut_val);
        }
    }

}//DFS_liek_2dalas


int mans_max_cut_3var(vertice *graph, const int vert_cnt){
    /// sakuma visas virsotnes izmetaa meginot salikt ka 2daligu grafu - tad parcel virsotnes, lidz vairak neuzlabojas
    int rez=0;
    bool *visited=new bool[vert_cnt];

    memset(visited, false, vert_cnt);

    for(int i=0; i<vert_cnt; ++i){
        if(!visited[i]){
            DFS_liek_2dalas(graph, visited, i, false);
        }
    }

    rez = mans_Max_cut_v_parcelajs(graph, vert_cnt);




    delete[] visited;

    return rez;
}//mans_max_cut_3var









int pilna_parlase_Max_cut(vertice *graph, const int vert_cnt){

    int best_rez=0;
    int cur_rez;

    if(vert_cnt>25){
        printf("\n\nVirsotnu skaits:%d par lielu, lai veiktu pilno parlasi!!\n", vert_cnt);
        return -1;
    }

    int var_cnt=(1<<vert_cnt);

    for(int cur_var=0, temp; cur_var<var_cnt; ++cur_var){
        temp=cur_var;
        cur_rez=0;
        for(int i=vert_cnt-1; i>=0; --i){
            graph[i].cut_val = (bool)(temp&1);
            temp>>=1;
        }

        for(int i=0; i<vert_cnt; ++i){
            for(edge e: graph[i].edges){
                if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                    cur_rez+=e.weight;
                }
            }
        }
        if(cur_rez>best_rez)best_rez=cur_rez;
    }

    return best_rez;
}//pilna_parlase_Max_cut



void analize1(vertice *graph){

    int max_rez, rez1,rez2,rez3;
    double proc1,proc2,proc3;

    int vert_cnt;

    int weights[21][21];
    int cnt_edges=0;

    fstream fin("in.txt",ios::in);
    int u,v;
    int cur_w;


    FILE *file_out=fopen("analizes1_rez.txt","w");

    uniform_int_distribution<int>my_unif_dist(15, 85);
    normal_distribution<double>my_n_dist(0.5, 0.3);
    int N=200;


    fprintf(file_out, "Analize 1 ar vienmerigo sadalijumu\n");
    //fprintf(file_out, "Analize 1 ar normalo sadalijumu\n");

    fprintf(file_out, "Grafu skaits: %d +1\n", N);


    //fprintf(file_out, "Nr       rez1%%     rez2%%     rez3%%    max_cut\n");
    fprintf(file_out, "Nr       rez1%%     rez2%%    max_cut\n");

    fin>>vert_cnt;
    while(fin>>u>>v>>cur_w){
        graph[u].edges.push_back(edge(v,cur_w));
        graph[v].edges.push_back(edge(u,cur_w));

        weights[u][v]=cur_w;
        weights[v][u]=cur_w;
        cnt_edges++;
    }
    fin.close();



    rez1=mans_max_cut_1var(graph, vert_cnt);
    rez2=mans_max_cut_2_2var(graph, vert_cnt);
    max_rez=mans_max_cut_3var(graph, vert_cnt);
    //rez2=mans_max_cut_2var(graph, vert_cnt);
    //rez3=mans_max_cut_3var(graph, vert_cnt);
    //max_rez=pilna_parlase_Max_cut(graph, vert_cnt);

    proc1 = ((double)(rez1)/(double)(max_rez))*100;
    proc2 = ((double)(rez2)/(double)(max_rez))*100;
    //proc3 = ((double)(rez3)/(double)(max_rez))*100;



    //fprintf(file_out," * %11.2f%10.2f%10.2f %10d\n", proc1, proc2, proc3, max_rez);
    fprintf(file_out," * %11.2f%10.2f %10d\n", proc1, proc2, max_rez);



    for(int i=1; i<=N; ++i){
        //int a=1000;
        //int b=0;
        printf("%3d/%3d  ... ", i, N);

        for(int v=0; v<vert_cnt; ++v){
            for(auto it = graph[v].edges.begin(); it!=graph[v].edges.end(); ++it){
                if(v<(*it).to_vertice){
                    cur_w=my_unif_dist(my_rand);

                    //cur_w = abs(round(my_n_dist(my_rand)*100));
                    //cur_w = round(my_n_dist(my_rand)*100);
                    //while(cur_w<0){cur_w = round(my_n_dist(my_rand)*100);}

                    weights[v][(*it).to_vertice]=cur_w;
                    weights[(*it).to_vertice][v]=cur_w;

                    (*it).weight=cur_w;

                    //if(cur_w<a)a=cur_w;
                    //if(cur_w>b)b=cur_w;
                    //printf("%d ",cur_w);
                }
                else{
                    (*it).weight = weights[(*it).to_vertice][v];
                }
            }
        }
        //printf("\n\n");

        rez1=mans_max_cut_1var(graph, vert_cnt);
        rez2=mans_max_cut_2_2var(graph, vert_cnt);
        max_rez=mans_max_cut_3var(graph, vert_cnt);
        //rez2=mans_max_cut_2var(graph, vert_cnt);
        //rez3=mans_max_cut_3var(graph, vert_cnt);
        //max_rez=pilna_parlase_Max_cut(graph, vert_cnt);

        proc1 = ((double)(rez1)/(double)(max_rez))*100;
        proc2 = ((double)(rez2)/(double)(max_rez))*100;
        //proc3 = ((double)(rez3)/(double)(max_rez))*100;


        //fprintf(file_out,"%2d %11.2f%10.2f%10.2f %10d\n",i, proc1, proc2, proc3, max_rez);
        fprintf(file_out,"%2d %11.2f%10.2f %10d\n",i, proc1, proc2, max_rez);
        //printf("min w=%d max w=%d\n",a,b);
        printf("\t[OK]\n");

    }


    fclose(file_out);

}//analize1



int main()
{
    const int max_vert_cnt=100;
    vertice *graph;
//    int vert_cnt;
//    int u,v;
//    int cur_w;
//
//    int tmp_rez, rez;





    graph=new vertice[max_vert_cnt];


    analize1(graph);


    return 0;
}//main
