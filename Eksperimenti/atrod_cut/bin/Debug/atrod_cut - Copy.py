import ctypes


_my_dll = ctypes.CDLL('libatrod_cut.dll')
_my_dll.graph_main_analize.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_double))


def graph_main_analize(virs_skaits, W, X):
    global _my_dll

    el_sk = virs_skaits * virs_skaits

    W_array_type = ctypes.c_int * el_sk
    X_array_type = ctypes.c_double * el_sk

    temp_w=[0]*el_sk
    temp_x=[0]*el_sk
    
    
    result = _my_dll.graph_main_analize(ctypes.c_int(virs_skaits), W_array_type(*temp_w), X_array_type(*X))
    return int(result)





N=4
W=[[1,2],[3,4]]
X=[[2.1,2.2],[2.3,2.4]]
graph_main_analize(N,W,X)


