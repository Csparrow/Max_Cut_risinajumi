import ctypes

_sum = ctypes.CDLL('libizm1.dll')
_sum.c_sum.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.c_int))

arr=[1,2,3,4,5,6]
array_type = ctypes.c_int * 6
print(array_type)
print(_sum.c_sum(ctypes.c_int(6), array_type(*arr)))
