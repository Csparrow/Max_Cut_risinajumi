#include <iostream>
#include <cstdio>
#include <random>
#include <fstream>
#include <cstring>

#include <Eigen/Dense>
#include <Eigen/Cholesky>

#include "asd1.h"



using namespace std;

uniform_real_distribution<double> my_dist2(-1,1);



int atrod_sdp_cut(vertice *graph,const int N, const Eigen::MatrixXf &V){
    int rez=0;

    Eigen::VectorXf r(N);
    //Eigen::VectorXd s(N);
    for(int i=0; i<N; ++i)r(i)=my_dist2(my_rand);
    r.normalize();


    for(int i=0; i<N; ++i){
        if(r.dot(V.row(i))>=0){
            //s(i)=1;
            graph[i].cut_val=0;
        }
        else{
            //s(i)=-1;
            graph[i].cut_val=1;
        }
    }

    for(int i=0; i<N; ++i){
        for(edge e: graph[i].edges){
            if(i<e.to_vertice && graph[i].cut_val!=graph[e.to_vertice].cut_val)rez+=e.weight;
        }
    }

    return rez;
}//atrod_sdp_cut



Eigen::MatrixXf atrod_V(const int N, const Eigen::MatrixXf &Y){
    Eigen::MatrixXf V(N,N);
    Eigen::LDLT<Eigen::MatrixXf> dekomp(Y);
    Eigen::MatrixXf D(N,N);
    Eigen::Transpositions<Eigen::Dynamic> P = dekomp.transpositionsP();
    Eigen::MatrixXf L=dekomp.matrixL();


    V = (P.transpose() * L);

    D = dekomp.vectorD().asDiagonal();

    if(dekomp.isPositive()){
        cout<<"qwe"<<endl;
    }
    else cout<<"tyu"<<endl;

    for(int i=0; i<N; ++i){
        if(D(i,i)<0){
            cout<<"asd"<<endl;
            cout<<D(i,i)<<endl;
            cout<<dekomp.vectorD()(i)<<endl;
            cin.get();
        }
        D(i,i) = sqrt(D(i,i));
    }

    V = V*D;
    cout<<V(12,12)<<endl;

    return V;
}
