#ifndef ASD1_H_INCLUDED
#define ASD1_H_INCLUDED

#include "max_cut_alg.h"

int atrod_sdp_cut(vertice *graph,const int N, const Eigen::MatrixXf &V);

Eigen::MatrixXf atrod_V(const int N, const Eigen::MatrixXf &Y);



#endif // ASD1_H_INCLUDED
