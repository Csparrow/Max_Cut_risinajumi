#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>
#include <ctime>

#include <random>

using namespace std;


/***
* Bez svariem
***/


mt19937 my_rand;




struct edge{
    int to_vertice;
    edge(int v){
        to_vertice=v;
    }
    edge(){}
};


struct vertice{
    vector<edge>edges;
    bool cut_val;
};




int random_Max_cut_1(vertice *graph, const int vert_cnt){

    int rez=0;
    //mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    edge e;

    //my_rand.seed(rand());


    for(int i=0; i<vert_cnt; ++i){
        //cut[i]=my_dist(my_rand);
        //printf("%d ",cut[i]);
        graph[i].cut_val = my_dist(my_rand);
    }
    //printf("  ");


    for(int i=0; i<vert_cnt; ++i){
        for(int j=0; j<graph[i].edges.size(); ++j){
            e=graph[i].edges[j];
            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez++;
            }
        }
    }
    //printf("cur rez:%.2f\n",rez);



    return rez;

}//random_Max_cut_1





int mans_Max_cut_2(vertice *graph, const int vert_cnt){
    int rez=0;


    int cur_diff, best_diff;
    pair<int,bool>*best_v=new pair<int,bool>[vert_cnt];
    int *w0=new int[vert_cnt];
    int *w1=new int[vert_cnt];
    int *p_no,*p_uz;
    int len;

    int chosen_vert_ind;
    bool cur_cut_side;

    uniform_int_distribution<int> my_dist;


    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=0;
        w0[i]=graph[i].edges.size();
        w1[i]=0;
    }




    while(true){

        best_diff=0;
        len=0;
        ///atrod 'max'
        for(int i=0; i<vert_cnt; ++i){
            cur_cut_side=graph[i].cut_val;
            if(cur_cut_side) cur_diff=w1[i]-w0[i];
            else cur_diff=w0[i]-w1[i];

            if(cur_diff>best_diff){
                len=1;
                best_v[0]=pair<int,bool>(i,cur_cut_side);
                best_diff=cur_diff;
            }
            else if(cur_diff==best_diff)best_v[len++]=pair<int,bool>(i,cur_cut_side);
        }



        if(best_diff>0){
            if(len==1){
                chosen_vert_ind = best_v[0].first;
                cur_cut_side = best_v[0].second;
            }
            else{
                int temp_ind=my_dist(my_rand)%len;
                chosen_vert_ind = best_v[temp_ind].first;
                cur_cut_side = best_v[temp_ind].second;
            }

            p_no=w0;
            p_uz=w1;
            if(cur_cut_side)swap(p_no, p_uz);



            for(edge e: graph[chosen_vert_ind].edges){
                p_no[e.to_vertice]--;
                p_uz[e.to_vertice]++;

            }
            graph[chosen_vert_ind].cut_val=!cur_cut_side;

        }
        else break;


    }




    for(int i=0; i<vert_cnt; ++i){
        for(edge e: graph[i].edges){
            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez++;
            }
        }
    }



    delete []best_v;
    delete []w0;
    delete []w1;

    return rez;
}//mans_Max_cut_2





int pilna_parlase_Max_cut(vertice *graph, const int vert_cnt){

    int best_rez=0;
    int cur_rez;

    if(vert_cnt>25){
        printf("\n\nVirsotnu skaits:%d par lielu, lai veiktu pilno parlasi!!\n", vert_cnt);
        return -1;
    }

    int var_cnt=(1<<vert_cnt);

    for(int cur_var=0, temp; cur_var<var_cnt; ++cur_var){
        temp=cur_var;
        cur_rez=0;
        for(int i=vert_cnt-1; i>=0; --i){
            graph[i].cut_val = (bool)(temp&1);
            temp>>=1;
        }

        for(int i=0; i<vert_cnt; ++i){
            for(edge e: graph[i].edges){
                if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                    cur_rez++;
                }
            }
        }
        if(cur_rez>best_rez)best_rez=cur_rez;
    }

    return best_rez;
}//pilna_parlase_Max_cut





pair<int,int> genere_grafu(vertice *graph, int vert_cnt, const double p1, const double p2){

    bernoulli_distribution dist_init(0.5);
    bernoulli_distribution dist1(p1);
    bernoulli_distribution dist2(p2);

    int cnt_edges=0;
    int tuvais_cut=0;



    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=dist_init(my_rand);
        graph[i].edges.clear();
    }


    for(int i=0; i<vert_cnt-1; ++i){
        for(int j=i+1; j<vert_cnt; ++j){
            if(graph[i].cut_val!=graph[j].cut_val){
                if(dist1(my_rand)){
                    graph[i].edges.push_back(edge(j));
                    graph[j].edges.push_back(edge(i));
                    ++cnt_edges;
                    ++tuvais_cut;
                }
            }
            else{
                if(dist2(my_rand)){
                    graph[i].edges.push_back(edge(j));
                    graph[j].edges.push_back(edge(i));
                    ++cnt_edges;
                }
            }
        }
    }


    //printf("Uzgenerets grafs ar %d skautnem   tuvais cut=%d\n\n",cnt_edges,tuvais_cut);

    return make_pair(cnt_edges, tuvais_cut);


}//genere_grafu





int main()
{
    vertice *graph;
    int vert_cnt=20;
    double p1=0.5,p2=0.25;

    int rez,cur_rez;
    pair<int,int>graph_val;


    graph=new vertice[1000000];









    int it_sk=10;
    int N=10;//30
    FILE *file_out=fopen("rez.txt","w");

    //p1=0.5;
    //p2=0.25;
    vert_cnt=10000;
    //0.8,0.1
    //0.6, 0.2
    //0.5, 0.25
    time_t a,b;



    p1=0.5;
    p2=0.25;
    fprintf(file_out, "### Grafs Vskaits:%d    P[in cut]=%.2f  P[out cut]=%.2f ###\n", vert_cnt, p1, p2);


    //fprintf(file_out, "Skautnu skaits       Sak cut        mans_rez     pilna parlase\n");
    fprintf(file_out, "Skautnu skaits       Sak cut        mans_rez     random it=%d\n",it_sk);



    for(int i=0; i<N; ++i){
        //time(&a);
        graph_val = genere_grafu(graph, vert_cnt, p1, p2);
        //time(&b);
        //cout<<"gen: "<<int(b-a)<<endl;


        fprintf(file_out, "%14d  %12d  ",graph_val.first, graph_val.second);

        //time(&a);
        rez=mans_Max_cut_2(graph, vert_cnt);
        fprintf(file_out,"%14d  ",rez);
        //time(&b);
        //cout<<"mans: "<<int(b-a)<<endl;


        //time(&a);
        rez=0;
        for(int a=0; a<it_sk; ++a){
            cur_rez=random_Max_cut_1(graph, vert_cnt);
            if(cur_rez>rez)rez=cur_rez;
        }
        fprintf(file_out,"%14d\n",rez);
        //time(&b);
        //cout<<"mans: "<<int(b-a)<<endl;



//        rez=pilna_parlase_Max_cut(graph,vert_cnt);
//        fprintf(file_out,"%14d\n",rez);

    }
    fprintf(file_out,"#############################################################\n");

    {
        p1=0.6;
        p2=0.2;
        fprintf(file_out, "\n### Grafs Vskaits:%d    P[in cut]=%.2f  P[out cut]=%.2f ###\n", vert_cnt, p1, p2);
        fprintf(file_out, "Skautnu skaits       Sak cut        mans_rez     random it=%d\n",it_sk);

        for(int i=0; i<N; ++i){
            graph_val = genere_grafu(graph, vert_cnt, p1, p2);

            fprintf(file_out, "%14d  %12d  ",graph_val.first, graph_val.second);

            rez=mans_Max_cut_2(graph, vert_cnt);
            fprintf(file_out,"%14d  ",rez);


            rez=0;
            for(int a=0; a<it_sk; ++a){
                cur_rez=random_Max_cut_1(graph, vert_cnt);
                if(cur_rez>rez)rez=cur_rez;
            }
            fprintf(file_out,"%14d\n",rez);



    //        rez=pilna_parlase_Max_cut(graph,vert_cnt);
    //        fprintf(file_out,"%14d\n",rez);

        }
        fprintf(file_out,"#############################################################\n");

        p1=0.8;
        p2=0.1;
        fprintf(file_out, "\n### Grafs Vskaits:%d    P[in cut]=%.2f  P[out cut]=%.2f ###\n", vert_cnt, p1, p2);
        fprintf(file_out, "Skautnu skaits       Sak cut        mans_rez     random it=%d\n",it_sk);


        for(int i=0; i<N; ++i){
            graph_val = genere_grafu(graph, vert_cnt, p1, p2);

            fprintf(file_out, "%14d  %12d  ",graph_val.first, graph_val.second);

            rez=mans_Max_cut_2(graph, vert_cnt);
            fprintf(file_out,"%14d  ",rez);


            rez=0;
            for(int a=0; a<it_sk; ++a){
                cur_rez=random_Max_cut_1(graph, vert_cnt);
                if(cur_rez>rez)rez=cur_rez;
            }
            fprintf(file_out,"%14d\n",rez);



    //        rez=pilna_parlase_Max_cut(graph,vert_cnt);
    //        fprintf(file_out,"%14d\n",rez);

        }
        fprintf(file_out,"#############################################################\n");
    }




    return 0;
}//main
