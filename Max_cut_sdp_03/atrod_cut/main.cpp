#include <iostream>
#include <cstdio>
#include <random>
#include <fstream>
#include <cstring>


#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <Eigen/Eigenvalues>

#include "max_cut_alg.h"



using namespace std;


string matricas_tips;

uniform_real_distribution<double> my_dist2(-1,1);

int atrod_sdp_cut(vertice *graph,const int N, const Eigen::MatrixXf &V){
    int rez=0;

    Eigen::VectorXf r(N);
    //Eigen::VectorXd s(N);
    for(int i=0; i<N; ++i)r(i)=my_dist2(my_rand);
    r.normalize();


    for(int i=0; i<N; ++i){
        if(r.dot(V.row(i))>=0){
            //s(i)=1;
            graph[i].cut_val=0;
        }
        else{
            //s(i)=-1;
            graph[i].cut_val=1;
        }
    }

    for(int i=0; i<N; ++i){
        for(edge e: graph[i].edges){
            if(i<e.to_vertice && graph[i].cut_val!=graph[e.to_vertice].cut_val)rez+=e.weight;
        }
    }

    return rez;
}//atrod_sdp_cut



Eigen::MatrixXf atrod_V(const int N, const Eigen::MatrixXf &X){
    Eigen::MatrixXf V(N,N);
    Eigen::LDLT<Eigen::MatrixXf> dekomp(X);
    Eigen::MatrixXf D(N,N);
    Eigen::Transpositions<Eigen::Dynamic> P = dekomp.transpositionsP();
    Eigen::MatrixXf L=dekomp.matrixL();


    V = (P.transpose() * L);

    D = dekomp.vectorD().asDiagonal();

    if(dekomp.isNegative()){
        matricas_tips="NS";
        return V;
    }
    else if(!dekomp.isPositive()){
        matricas_tips="undef";
        return V;
    }
    else matricas_tips="PS";

    for(int i=0; i<N; ++i){
        D(i,i) = sqrt(D(i,i));
    }

    V = V*D;

    return V;
}//atrod_V




extern "C"
{


    int graph_main_analize(int virs_sk, int **w, double **orig_x){


//        for(int i=0; i<virs_sk; ++i){
//            for(int j=0; j<virs_sk; ++j)printf("%d ",w[i][j]);
//            printf("\n");
//        }
//        printf("\n\n");
//        for(int i=0; i<virs_sk; ++i){
//            for(int j=0; j<virs_sk; ++j)printf("%f ",orig_x[i][j]);
//            printf("\n");
//        }
//        printf("\n\n");


        matricas_tips="?";
        const int N=virs_sk;
        vertice *graph=new vertice[10000];
        double a;


        int cur_cut;

        FILE *f=fopen("cut_rez.txt","a");

        Eigen::MatrixXf X(N,N);
        Eigen::MatrixXf V(N,N);


        for(int i=0,cur_w; i<N; ++i){
            for(int j=i+1; j<N;++j){
                if(w[i][j]>0){
                    cur_w=w[i][j];
                    graph[i].edges.push_back(edge(j,cur_w));
                    graph[j].edges.push_back(edge(i,cur_w));
                }
            }
        }

        for(int i=0; i<N; ++i){
            for(int j=i; j<N;++j){
                a=orig_x[i][j];
                X(i,j)=a;
                X(j,i)=a;
            }
        }





        ///

        cout<<endl<<X<<endl<<endl;
        Eigen::VectorXcf v1 = X.eigenvalues();
        cout<<endl<<"eigenvalues of x: "<<v1<<endl;
        return -1;
        ///


        V = atrod_V(N, X);

        if(matricas_tips!="PS"){
            printf("nepareiza X matrica\n");
            printf("Matrica:%s\n",matricas_tips.c_str());
            return -1;
        }




        /*
        Eigen::MatrixXf temp=V*V.transpose();

        for(int i=0; i<N; ++i){
            for(int j=0; j<N;++j){
                printf("%.2f ",X(i,j));
            }
            printf("\n");
        }
        printf("\n");

        for(int i=0; i<N; ++i){
            for(int j=0; j<N;++j){
                printf("%.2f ",temp(i,j));
            }
            printf("\n");
        }
        printf("\n");
        */






        fprintf(f,"%d ", mans_max_cut_1var(graph, N));
        fprintf(f,"%d ",mans_max_cut_2_2var(graph,N, 100));
        fprintf(f,"%d ", mans_max_cut_3var(graph,N));
        fprintf(f,"%d ", mans_Max_cut_random_v_parcelajs(graph,N));
        fprintf(f,"%d ", random_Max_cut_1(graph, N, 500));

        for(int i=0; i<10; ++i) fprintf(f,"%d ",atrod_sdp_cut(graph, N, V));
        fprintf(f,"\n");

        delete[] graph;


        return 0;
    }
}
