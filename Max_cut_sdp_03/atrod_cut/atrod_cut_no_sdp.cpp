#include <iostream>
#include <cstdio>
#include <random>
#include <fstream>
#include <cstring>

#include <Eigen/Dense>
#include <Eigen/Cholesky>

#include "max_cut_alg.h"
#include "max_cut_alg.cpp"


using namespace std;

uniform_real_distribution<double> my_dist2(-1,1);


int atrod_sdp_cut(vertice *graph,const int N, const Eigen::MatrixXf &V){
    int rez=0;

    Eigen::VectorXf r(N);
    //Eigen::VectorXd s(N);
    for(int i=0; i<N; ++i)r(i)=my_dist2(my_rand);
    r.normalize();


    for(int i=0; i<N; ++i){
        if(r.dot(V.row(i))>=0){
            //s(i)=1;
            graph[i].cut_val=0;
        }
        else{
            //s(i)=-1;
            graph[i].cut_val=1;
        }
    }

    for(int i=0; i<N; ++i){
        for(edge e: graph[i].edges){
            if(i<e.to_vertice && graph[i].cut_val!=graph[e.to_vertice].cut_val)rez+=e.weight;
        }
    }

    return rez;
}//atrod_sdp_cut



Eigen::MatrixXf atrod_V(const int N, const Eigen::MatrixXf &Y){
    Eigen::MatrixXf V(N,N);
    Eigen::LDLT<Eigen::MatrixXf> dekomp(Y);
    Eigen::MatrixXf D(N,N);
    Eigen::Transpositions<Eigen::Dynamic> P = dekomp.transpositionsP();
    Eigen::MatrixXf L=dekomp.matrixL();


    V = (P.transpose() * L);

    D = dekomp.vectorD().asDiagonal();

    if(dekomp.isPositive()){
        cout<<"qwe"<<endl;
    }
    else cout<<"tyu"<<endl;

    for(int i=0; i<N; ++i){
        if(D(i,i)<0){
            cout<<"asd"<<endl;
            cout<<D(i,i)<<endl;
            cout<<dekomp.vectorD()(i)<<endl;
            cin.get();
        }
        D(i,i) = sqrt(D(i,i));
    }

    V = V*D;
    cout<<V(12,12)<<endl;

    return V;
}


int main(int argc, char **argv)
{

  fstream fin;
  int N;
  double a;
  vertice *graph=new vertice[10000];
  int v1,v2,w;

  char fname[200]="";


  int cur_cut;


  strcpy(fname,"graph.txt");

//  if(argc!=2){
//    cout<<"Kluda!"<<endl;
//    return -1;
//  }
//  else strcpy(fname,argv[1]);



  fin.open(fname,ios::in);
  FILE *f=fopen("cut_rez.txt","a");
  fin>>N;

  Eigen::MatrixXf Y(N,N);
  Eigen::MatrixXf V(N,N);





  while(fin>>v1>>v2>>w){
    graph[v1].edges.push_back(edge(v2,w));
    graph[v2].edges.push_back(edge(v1,w));
  }
  fin.close();


  fin.open("Y_matrica.txt",ios::in);
  for(int i=0; i<N; ++i){
    for(int j=0; j<N;++j){
        fin>>a;
        Y(i,j)=a;
    }
  }
  fin.close();



  V=atrod_V(N, Y);





  /*
  Eigen::MatrixXf temp=V*V.transpose();
  for(int a=0; a<N;++a){
    for(int b=0; b<N; ++b){
        if(temp(a,b)!=Y(a,b)){
            cout<<"NOT"<<endl;
            return -1;
        }
    }
  }
  cout<<"OK"<<endl;
  return 0;
  */





  fprintf(f,"%d ", mans_max_cut_1var(graph, N));
  fprintf(f,"%d ",mans_max_cut_2_2var(graph,N, 100));
  fprintf(f,"%d ", mans_max_cut_3var(graph,N));
  fprintf(f,"%d ", mans_Max_cut_random_v_parcelajs(graph,N));
  fprintf(f,"%d ", random_Max_cut_1(graph, N, 500));

  for(int i=0; i<10; ++i) fprintf(f,"%d ",atrod_sdp_cut(graph, N, V));
  fprintf(f,"\n");



  return 0;
}//main
