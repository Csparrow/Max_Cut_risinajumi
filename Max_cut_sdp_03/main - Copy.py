import picos
import cvxopt

import atrod_cut


def atrod_lapalasa_m(W,N):
    e = cvxopt.matrix(1, (N,1))
    v=W*e
    m1=cvxopt.matrix(0, (N,N))
    for i in range(N):
        m1[i,i]=v[i,0]
    rez = m1-W
    return rez



with open("graph.txt","r") as f:
    N = int(f.readline())
    W = cvxopt.matrix(0, (N, N))

    for line in f:
        if(line=="\n" or line=="\t"):continue
        a,b,w=[int(x) for x in line.split()]
        W[a,b]=w
        W[b,a]=w

LL = 0.25*(atrod_lapalasa_m(W,N))

max_cut_sdp = picos.Problem()

X=max_cut_sdp.add_variable('X',(N,N),'symmetric')

L=picos.new_param('L',LL)

#max_cut_sdp.add_constraint(picos.tools.diag_vect(X)==1)

#####
epsilon=1.0e-10000000000000000000
E=cvxopt.matrix(0, (N, N))
for i in range(N):
    E[i,i]=1
#max_cut_sdp.add_constraint((E|X)<1+epsilon and (E|X)>1-epsilon)
max_cut_sdp.add_constraint(picos.tools.diag_vect(X)==1)
######

max_cut_sdp.add_constraint(X>>0)


max_cut_sdp.set_objective('max',L|X)



max_cut_sdp.solve(solver='cvxopt',verbose=0)
#max_cut_sdp.solve(solver='smcp')

X_matrix = X.value


#print(X_matrix)
##########################

atrod_cut.graph_main_analize(N, W, X_matrix)












