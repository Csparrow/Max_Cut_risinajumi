import cvxopt
import numpy
import numpy.matlib
import picos

a=picos.Problem()

X=a.add_variable('X',(4,4),'symmetric')

a.add_constraint(picos.tools.diag_vect(X)==1)
a.add_constraint(X>>0)


print(a)
