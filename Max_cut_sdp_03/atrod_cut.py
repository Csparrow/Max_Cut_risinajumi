import ctypes


_my_dll = ctypes.CDLL('libatrod_cut.dll')
_my_dll.graph_main_analize.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.POINTER(ctypes.c_int)), ctypes.POINTER(ctypes.POINTER(ctypes.c_double)))


def graph_main_analize(virs_skaits, W, X):
    global _my_dll


    int_n_arr=ctypes.c_int*virs_skaits
    p_int_n_arr=ctypes.POINTER(ctypes.c_int)*virs_skaits

    temp_w = p_int_n_arr()
    for i in range(virs_skaits):
        temp_w[i] = int_n_arr()
        for j in range(virs_skaits):
            #temp_w[i][j] = W[i,j]
            temp_w[i][j] = W[i][j]
            


    double_n_arr = ctypes.c_double * virs_skaits
    p_double_n_arr = ctypes.POINTER(ctypes.c_double) * virs_skaits

    temp_x = p_double_n_arr()
    for i in range(virs_skaits):
        temp_x[i] = double_n_arr()
        for j in range(virs_skaits):
            #temp_x[i][j] = X[i,j]
            temp_x[i][j] = X[i][j]


    result=0
    result = _my_dll.graph_main_analize(ctypes.c_int(virs_skaits), temp_w, temp_x)

    return int(result)




''' 
N=2
W=[[1, 2], [3, 4]]
X=[[1.0, -1.0], [-1.0, 1.0]]

graph_main_analize(N, W, X)
'''


