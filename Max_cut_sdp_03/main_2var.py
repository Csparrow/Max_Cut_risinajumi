import mosek
from   mosek.fusion import *
import numpy as np
import atrod_cut
import ch_f



def atrod_lapalasa_m(W,N):
    e = np.ones((N,1))
    v=W*e
    m1=np.zeros((N,N))
    for i in range(N):
        m1[i][i]=v[i][0]
    rez = m1-W
    return rez





with open("graph.txt","r") as f:
    N = int(f.readline())
    W = np.zeros((N, N),dtype=int)

    for line in f:
        if(line=="\n" or line=="\t"):continue
        a,b,w=[int(x) for x in line.split()]
        W[a][b]=w
        W[b][a]=w





LL = 0.25*(atrod_lapalasa_m(W,N))



rez_X=np.zeros((N,N))

with Model("max cut") as M:
    X = M.variable("X",Domain.inPSDCone(N))
    C = Matrix.dense(LL)
    I = Matrix.eye(N)



    M.objective(ObjectiveSense.Maximize, Expr.dot(C,X))

    M.constraint("c1", Expr.mulDiag(X,I), Domain.equalsTo(1.0))
    

    M.solve()

    temp=X.level()
    k=0
    for i in range(N):
        for j in range(N):
            rez_X[i][j]=temp[k]
            k+=1




print(rez_X,"\n\n\n")

#print("\n\n",np.linalg.eig(rez_X))
#print("\n\n\n",np.linalg.eigvals(rez_X))
#print("\n\n\n\n")
#atrod_cut.graph_main_analize(N,W,rez_X)

V = ch_f.ch_f(rez_X,N)

print(V*V.transpose())




