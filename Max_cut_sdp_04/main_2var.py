import mosek
from   mosek.fusion import *
import numpy as np
from subprocess import run
import os
import ch_f
import atrod_cur_sdp_cut
import time



def atrod_lapalasa_m(W,N):
    e = np.ones((N,1))
    v=W*e
    m1=np.zeros((N,N))
    for i in range(N):
        m1[i][i]=v[i][0]
    rez = m1-W
    return rez



def atrod_cut(fname, iter_skaits=20):

    with open(fname,"r") as f:
        N = int(f.readline())
        W = np.zeros((N, N),dtype=int)

        for line in f:
            if(line=="\n" or line=="\t"):continue
            a,b,w=[int(x) for x in line.split()]
            W[a][b]=w
            W[b][a]=w





    LL = 0.25*(atrod_lapalasa_m(W,N))



    rez_X=np.zeros((N,N))

    with Model("max cut") as M:
        X = M.variable("X",Domain.inPSDCone(N))
        C = Matrix.dense(LL)
        I = Matrix.eye(N)



        M.objective(ObjectiveSense.Maximize, Expr.dot(C,X))

        M.constraint("c1", Expr.mulDiag(X,I), Domain.equalsTo(1.0))
        

        M.solve()

        temp=X.level()
        k=0
        for i in range(N):
            for j in range(N):
                rez_X[i][j]=temp[k]
                k+=1


    V = ch_f.ch_f(rez_X,N)

    #print(rez_X,"\n\n\n")
    #print(V*V.transpose())
    atrod_cur_sdp_cut.atrod_cur_sdp_cut(W,V,N,iter_skaits)

########################################






"""
dir_name="E:\\LU_DF\\Sem_6\\Kursa_Darbs\\grafu_f\\eksp2"

iter_skaits=1

skaits=20
virs_sk=1000

print("start: ",time.ctime())

fname=dir_name+"\\3dal_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    atrod_cut(fname.format(i), iter_skaits)
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_3dal_{0}v.txt".format(virs_sk))
print("beidzas 3dal")




fname=dir_name+"\\rand1_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    atrod_cut(fname.format(i), iter_skaits)
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_rand1_{0}v.txt".format(virs_sk))
print("beidzas rand1")


fname=dir_name+"\\rand2_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    atrod_cut(fname.format(i), iter_skaits)
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_rand2_{0}v.txt".format(virs_sk))
print("beidzas rand2")

fname=dir_name+"\\reg3_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    atrod_cut(fname.format(i), iter_skaits)
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_reg3_{0}v.txt".format(virs_sk))
print("beidzas reg3")

fname=dir_name+"\\reg4_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    atrod_cut(fname.format(i), iter_skaits)
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_reg4_{0}v.txt".format(virs_sk))
print("beidzas reg4")

print("end: ",time.ctime())




#
#fname=dir_name+"\\eksp2\\rand2_20_{0}.txt"
#fname=dir_name+"\\eksp2\\reg3_20_{0}.txt"
#fname=dir_name+"\\eksp2\\reg4_20_{0}.txt"





fname=dir_name+"\\eksp2\\3dal_20_{0}.txt"
#fname=dir_name+"\\eksp2\\rand1_20_{0}.txt"
#fname=dir_name+"\\eksp2\\rand2_20_{0}.txt"
#fname=dir_name+"\\eksp2\\reg3_20_{0}.txt"
#fname=dir_name+"\\eksp2\\reg4_20_{0}.txt"

skaits=100

for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    atrod_cut(fname.format(i), iter_skaits)
    print("[OK]")

"""





























