from subprocess import run
import os
import time

from main_2var import atrod_cut



def analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, pref_name):
    
    fname=dir_name+"\\" + pref_name +"_{0}".format(virs_sk)+"_{0}.txt"

    for i in range(skaits):
        print("{0}/{1}".format(i+1,skaits),end="\t")
        atrod_cut(fname.format(i), iter_skaits)
        atrod_cut(fname.format(i), 1)
        run("atrod_classic_cut "+fname.format(i))
        print("[OK]")
    os.rename("cut_rez.txt","cut_rez_{0}_{1}v.txt".format(pref_name, virs_sk))
    print("beidzas {0}".format(pref_name))




def temp_analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, pref_name):
    
    fname=dir_name+"\\" + pref_name +"_{0}".format(virs_sk)+"_{0}.txt"

    for i in range(skaits):
        print("{0}/{1}".format(i+1,skaits),end="\t")
        t1 = time.time()
        run("atrod_classic_cut "+fname.format(i))
        t2=time.time()
        print(int(t2-t1),end=" ")
        t1 = time.time()
        atrod_cut(fname.format(i), iter_skaits)
        t2=time.time()
        print(int(t2-t1),end="  ")
        print("[OK]")
    os.rename("cut_rez.txt","cut_rez_{0}_{1}v.txt".format(pref_name, virs_sk))
    print("beidzas {0}".format(pref_name))

    


def analize_classic(dir_name, skaits, virs_sk, pref_name):
    
    fname=dir_name+"\\" + pref_name +"_{0}".format(virs_sk)+"_{0}.txt"

    for i in range(skaits):
        print("{0}/{1}".format(i+1,skaits),end="\t")
        run("atrod_classic_cut "+fname.format(i))
        print("[OK]")
    os.rename("cut_rez.txt","cut_rez_{0}_{1}v.txt".format(pref_name, virs_sk))
    print("beidzas {0}".format(pref_name))


    







dir_name="E:\\LU_DF\\Sem_6\\Kursa_Darbs\\grafu_f\\eksp2"

iter_skaits=20

skaits=100
virs_sk=500

prefl=["3dal", "rand1", "rand2", "reg3", "reg4"]


#for pref_name in prefl: analize_classic(dir_name, skaits, virs_sk, pref_name)

for pref_name in prefl: analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, pref_name)








'''
temp_analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "3dal")
temp_analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "rand1")
temp_analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "rand2")
temp_analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "reg3")
temp_analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "reg4")
'''


'''
print("start: ",time.ctime())

analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "3dal")
analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "rand1")
analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "rand2")
analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "reg3")
analize_classic_SDP(dir_name, skaits, virs_sk, iter_skaits, "reg4")

print("end: ", time.ctime())
'''
