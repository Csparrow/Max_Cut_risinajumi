import os
import time
from subprocess import run

'''
palaiz un analize tikai classic cut
'''

dir_name="E:\\LU_DF\\Sem_6\\Kursa_Darbs\\grafu_f\\eksp2"

skaits=20
virs_sk=1000

print("start: ",time.ctime())

fname=dir_name+"\\3dal_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_3dal_{0}v.txt".format(virs_sk))
print("beidzas 3dal")




fname=dir_name+"\\rand1_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_rand1_{0}v.txt".format(virs_sk))
print("beidzas rand1")


fname=dir_name+"\\rand2_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_rand2_{0}v.txt".format(virs_sk))
print("beidzas rand2")

fname=dir_name+"\\reg3_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_reg3_{0}v.txt".format(virs_sk))
print("beidzas reg3")

fname=dir_name+"\\reg4_{0}".format(virs_sk)+"_{0}.txt"
for i in range(skaits):
    print("{0}/{1}".format(i+1,skaits),end="\t")
    run("atrod_classic_cut "+fname.format(i))
    
    print("[OK]")
os.rename("cut_rez.txt","cut_rez_reg4_{0}v.txt".format(virs_sk))
print("beidzas reg4")

print("end: ",time.ctime())
