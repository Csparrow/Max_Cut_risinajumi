import random
import numpy as np


def atrod_cur_sdp_cut(weights,V,N, iter_skaits=20):

    r=np.zeros((N,))
    s=np.zeros((N,))

    V_arr = np.array(V)

    cur_cut=0

    with open("cut_rez.txt","a") as f:

        for q in range(iter_skaits):
            ###
            for i in range(N):
                r[i] = random.uniform(-1, 1)
            norm = np.linalg.norm(r)
            for i in range(N):
                r[i] = r[i] / norm

            for i in range(N):
                if (np.dot(r, V_arr[i]) >= 0):
                    s[i] = 1
                else:
                    s[i] = -1

            cur_cut = 0
            for i in range(N):
                for j in range(i + 1, N):
                    if (s[i] != s[j]): cur_cut += weights[i][j]
            print(cur_cut, end=" ",file=f)
            #print(cur_cut, end=" ")
                    
            ###
        #print("\n")
        print("",end=" ", file=f)



