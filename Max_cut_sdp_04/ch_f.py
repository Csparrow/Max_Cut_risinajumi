import mosek
import numpy


def atrodL(n, perm, diag, lnzc, lptrc, lensubnval, lsubc, lvalc):

    l = [[0.0 for j in range(n)] for i in range(n)]
    for j in range(n):
        for i in range(lptrc[j], lptrc[j] + lnzc[j]):
            l[lsubc[i]][j] = lvalc[i]
    L=numpy.matrix(l)
    #print("P = ", perm)
    #print("diag(D) = ", numpy.array(diag))
    #print("L=\n",L)
    return(L)

    


def ch_f(A,N):

    with mosek.Env() as env:

        anzc=[N-i for i in range(N)]
        asubc=[]
        for i in range(N):
            for j in range(i,N):
                asubc.append(j)
        
        aptrc=[0]*N
        t1=0
        for i in range(N):
            aptrc[i]=t1
            t1+=(N-i)
        
        avalc=[]
        for i in range(N):
            for j in range(i,N):
                avalc.append(A[i][j])



        try:
            perm, diag, lnzc, lptrc, lensubnval, lsubc, lvalc = env.computesparsecholesky(
                0,      #Disable multithread
                0,      #User reordering heuristic
                1.0e-14,#Singularity tolerance
                anzc, aptrc, asubc, avalc)

            rezL = atrodL(N, perm, diag, lnzc, lptrc, lensubnval, lsubc, lvalc)
            return rezL
        except:
            raise
    

