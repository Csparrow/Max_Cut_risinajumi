#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>
#include <ctime>

#include <random>

#include "max_cut_alg.h"


#include "max_cut_random_walk.hpp"


#define TEST 0

///

void druka_adj_M(vertice *graph, int vert_cnt){
    vector<vector<int>>M;
    M.resize(vert_cnt,vector<int>(vert_cnt,0));

    for(int i=0; i<vert_cnt; ++i){
        for(edge e:graph[i].edges){
            M[i][e.to_vertice]=e.weight;
            M[e.to_vertice][i]=e.weight;
        }
    }

    for(row:M){
        for(el:row){
            printf("%d ",el);
        }
        printf("\n");
    }
}








int D2Greedy(vertice *graph, int vert_cnt){
    int rez=0;

    int prev_S_cut,cur_S_cut;
    int prev_T_cut,cur_T_cut;
    int a,b;

    bool *S=new bool[vert_cnt];
    bool *T=new bool[vert_cnt];

    memset(S,false,vert_cnt);
    memset(T,true,vert_cnt);

    prev_S_cut=0;
    prev_T_cut=0;

    for(int i=0; i<vert_cnt; ++i){
        cur_S_cut=prev_S_cut;
        cur_T_cut=prev_T_cut;

        for(edge e: graph[i].edges){
            if(!S[e.to_vertice]) cur_S_cut+=e.weight;
            else cur_S_cut-=e.weight;

            if(T[e.to_vertice]) cur_T_cut+=e.weight;
            else cur_T_cut-=e.weight;
        }
        a=cur_S_cut-prev_S_cut;
        b=cur_T_cut-prev_T_cut;

        if(a>=b){
            S[i]=true;
            prev_S_cut=cur_S_cut;
        }
        else {
            T[i]=false;
            prev_T_cut=cur_T_cut;
        }
    }

    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=S[i];
    }

    rez=prev_S_cut;


//    int qw=0;
//    for(int i=0; i<vert_cnt; ++i){
//        for(edge e: graph[i].edges){
//            if(i<e.to_vertice && graph[i].cut_val!=graph[e.to_vertice].cut_val)qw+=e.weight;
//        }
//    }
//    if(qw==rez)printf("OK\n");
//    else printf("NOT\n");



    delete[]S;
    delete[]T;

    return rez;
}//D2Greedy




int SG(vertice *graph, int vert_cnt){
    int rez=0;
    char *S=new char[vert_cnt];
    int x,y;
    int w1,w2;

    int maxw=-1;
    for(int i=0; i<vert_cnt; ++i){
        S[i]='*';
        for(edge e: graph[i].edges){
            if(e.weight>maxw){
                maxw=e.weight;
                x=i;
                y=e.to_vertice;
            }
        }
    }


    S[x]='1';
    S[y]='2';
    graph[x].cut_val=false;
    graph[y].cut_val=true;
    rez=maxw;


    for(int i=0; i<vert_cnt; ++i){
        if(i==x || i==y)continue;
        w1=0;
        w2=0;
        for(edge e: graph[i].edges){
            if(S[e.to_vertice]=='1') w1+=e.weight;
            else if(S[e.to_vertice]=='2') w2+=e.weight;

        }

        if(w1>w2){
            S[i]='2';
            graph[i].cut_val=true;
        }
        else{
            S[i]='1';
            graph[i].cut_val=false;
        }

        rez += max(w1,w2);
    }

    delete[]S;
    return rez;
}//SG




int RDGreedy(vertice *graph, int vert_cnt){

    bernoulli_distribution bdist;
    int rez=0;

    int prev_S_cut,cur_S_cut;
    int prev_T_cut,cur_T_cut;
    int a,b;
    int a_,b_;
    double prob;

    bool *S=new bool[vert_cnt];
    bool *T=new bool[vert_cnt];

    memset(S,false,vert_cnt);
    memset(T,true,vert_cnt);

    prev_S_cut=0;
    prev_T_cut=0;

    for(int i=0; i<vert_cnt; ++i){
        cur_S_cut=prev_S_cut;
        cur_T_cut=prev_T_cut;

        for(edge e: graph[i].edges){
            if(!S[e.to_vertice]) cur_S_cut+=e.weight;
            else cur_S_cut-=e.weight;

            if(T[e.to_vertice]) cur_T_cut+=e.weight;
            else cur_T_cut-=e.weight;
        }
        a=cur_S_cut-prev_S_cut;
        b=cur_T_cut-prev_T_cut;

        a_=max(a,0);
        b_=max(b,0);
        if(a_== b_ && a_==0)prob=1.0;
        else prob = double(a_)/double(a_+b_);

        bdist = bernoulli_distribution (prob);

        if(bdist(my_rand)){
            S[i]=true;
            prev_S_cut=cur_S_cut;
        }
        else {
            T[i]=false;
            prev_T_cut=cur_T_cut;
        }
    }

    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=S[i];
    }

    rez=prev_S_cut;


    delete[]S;
    delete[]T;

    return rez;
}//RDGreedy





int SG3(vertice *graph, int vert_cnt){
    int rez=0;
    char *S=new char[vert_cnt];
    int *w1=new int[vert_cnt];
    int *w2=new int[vert_cnt];
    int x,y;
    int max_score,cur_score,j;


    int maxw=-1;
    for(int i=0; i<vert_cnt; ++i){
        S[i]='*';
        w1[i]=0;
        w2[i]=0;
        for(edge e: graph[i].edges){
            if(e.weight>maxw){
                maxw=e.weight;
                x=i;
                y=e.to_vertice;
            }
        }
    }


    S[x]='1';
    S[y]='2';
    graph[x].cut_val=false;
    graph[y].cut_val=true;
    rez=maxw;

    for(edge e:graph[x].edges) w1[e.to_vertice]=e.weight;
    for(edge e:graph[y].edges) w2[e.to_vertice]=e.weight;

    for(int i=0; i<vert_cnt-2; ++i){
        max_score=0;
        for(int k=0; k<vert_cnt; ++k){
            if(S[k]!='*')continue;

            cur_score=abs(w1[k]-w2[k]);
            if(cur_score>=max_score){
                max_score=cur_score;
                j=k;
            }
        }
        if(w1[j]>w2[j]){
            S[j]='2';
            graph[j].cut_val=true;
            for(edge e:graph[j].edges) w2[e.to_vertice]+=e.weight;
        }
        else{
            S[j]='1';
            graph[j].cut_val=false;
            for(edge e:graph[j].edges) w1[e.to_vertice]+=e.weight;
        }


        rez+=max(w1[j],w2[j]);
    }

    delete[]S;
    delete[]w1;
    delete[]w2;
    return rez;
}//SG3





int EC(vertice *graph, int vert_cnt){
    int rez=0;

    vector<vector<int>>adj_m(vert_cnt,vector<int>(vert_cnt,0));

    pair<int,int>min_uv;
    int min_w;
    int cur_skautnu_sk;

    vector<vector<int>>contr_list(vert_cnt);
    for(int i=0; i<vert_cnt; ++i)contr_list[i].push_back(i);


    for(int i=0; i<vert_cnt; ++i)graph[i].cut_val=false;



    for(int i=0; i<vert_cnt; ++i){
        for(edge e: graph[i].edges){
            adj_m[i][e.to_vertice]=e.weight;
        }
    }

    for(int i=0; i<vert_cnt-2; ++i){


        cur_skautnu_sk=0;
        for(int a=0; a<vert_cnt; ++a){
            for(int b=a+1; b<vert_cnt; ++b){
                if(adj_m[a][b]>0){
                    cur_skautnu_sk++;
                    if(adj_m[a][b]<min_w || cur_skautnu_sk==1){
                        min_w=adj_m[a][b];
                        min_uv = make_pair(a,b);
                    }
                }
            }
        }

        if(cur_skautnu_sk<2)break;//palikusi tikai viena vai neviena skautne



        adj_m[min_uv.first][min_uv.second]=0;
        adj_m[min_uv.second][min_uv.first]=0;

        contr_list[min_uv.first].insert(contr_list[min_uv.first].end(), contr_list[min_uv.second].begin(), contr_list[min_uv.second].end());
        for(int a=0; a<vert_cnt; ++a){
            adj_m[min_uv.first][a]+=adj_m[min_uv.second][a];
            adj_m[a][min_uv.first]=adj_m[min_uv.first][a];

            adj_m[min_uv.second][a]=0;
            adj_m[a][min_uv.second]=0;
        }

        /*
        printf("izmet %d %d\n",min_uv.first,min_uv.second);
        for(int a=0; a<vert_cnt; ++a){
            for(auto x:contr_list[a])printf("%d ",x);
            printf("\n");
        }
        printf("\n");
        */

        /*
        for(int i=0; i<vert_cnt;++i){
            for(int j=0;j<vert_cnt;++j){
                printf("%d ",adj_m[i][j]);
            }
            printf("\n");
        }
        system("pause");
        system("cls");
        */



    }

    for(auto x:contr_list[min_uv.first])graph[x].cut_val=true;



    for(int i=0; i<vert_cnt; ++i){
        for(edge e: graph[i].edges){
            if(i<e.to_vertice && graph[i].cut_val!=graph[e.to_vertice].cut_val)rez+=e.weight;
        }
    }



    return rez;
}//EC





///



using namespace std;

int main(int argc, char **argv)
{
    char in_fname[200];
    fstream fin,fout;
    int N;
    int a,b,w;
    vertice *graph;

    if(argc==2){
        sscanf(argv[1],"%s",in_fname);
    }
    else return -1;

    fin.open(in_fname,ios::in);
    fout.open("cut_rez.txt",ios::app);

    fin>>N;
    graph=new vertice[N];
    while(fin>>a>>b>>w){
        graph[a].edges.push_back(edge(b,w));
        graph[b].edges.push_back(edge(a,w));
    }

    max_cut_random_walk my_random_walk_cut;





# if TEST==1


//    printf("dfs var:%d\n\n",mans_max_cut_3var(graph,N));
//    printf("D2Greedy:%d\n\n",D2Greedy(graph,N));
//    printf("RDGreedy:%d\n\n",RDGreedy(graph,N));
//    printf("SG:%d\n\n",SG(graph,N));
//    printf("SG3:%d\n\n",SG3(graph,N));
//    printf("EC: %d\n",EC(graph,N));





    //printf("random walk cut/parc: %.2f\n",double(my_random_walk_cut.atrod_cut2(graph,N))/double(mans_max_cut_1var(graph, N)) );
    int qw=N*N;
//    printf("random walk cut/dfs: %.2f\n",double(my_random_walk_cut.atrod_cut2(graph,N, qw))/double(mans_max_cut_3var(graph, N)) );
//    printf("random walk cut/dfs: %.2f\n",double(my_random_walk_cut.atrod_cut2(graph,N, qw))/double(mans_max_cut_3var(graph, N)) );
//    printf("random walk cut/dfs: %.2f\n",double(my_random_walk_cut.atrod_cut2(graph,N, qw))/double(mans_max_cut_3var(graph, N)) );
//    printf("random walk cut/dfs: %.2f\n",double(my_random_walk_cut.atrod_cut2(graph,N, qw))/double(mans_max_cut_3var(graph, N)) );
//    printf("random walk cut/dfs: %.2f\n",double(my_random_walk_cut.atrod_cut2(graph,N, qw))/double(mans_max_cut_3var(graph, N)) );
//    printf("random walk cut/dfs: %.2f\n",double(my_random_walk_cut.atrod_cut2(graph,N, qw))/double(mans_max_cut_3var(graph, N)) );



//    printf("%d\n",pilna_parlase_Max_cut(graph, N));
//    printf("%d\n",mans_max_cut_1var(graph, N));
//    for(int i=0; i<N; ++i)printf("%d: %d\n",i,graph[i].cut_val);
//    printf("%d\n",mans_max_cut_3var(graph, N));
//    for(int i=0; i<N; ++i)printf("%d: %d\n",i,graph[i].cut_val);
//    druka_adj_M(graph, N);


//    printf("d2g/dfs: %.2f\n",double(D2Greedy(graph,N))/double(mans_max_cut_3var(graph,N)));
//    printf("rdg/dfs: %.2f\n",double(RDGreedy(graph,N))/double(mans_max_cut_3var(graph,N)));


//////////////////////////////////////////////////////////////////////////////////////////////////////////

#else





    fout<<mans_max_cut_1var(graph, N)<<" ";
    fout<<mans_max_cut_2_2var(graph,N, 100)<<" ";
    fout<<mans_max_cut_3var(graph,N)<<" ";
    fout<<mans_Max_cut_random_v_parcelajs(graph,N)<<" ";
    fout<<random_Max_cut_1(graph, N, 500)<<" ";
    fout<<mans_max_cut_2_2var(graph,N, 1)<<" ";
    fout<<random_Max_cut_1(graph, N, 1)<<" ";


    fout<<D2Greedy(graph,N)<<" ";
    fout<<RDGreedy(graph,N)<<" ";
    fout<<SG(graph,N)<<" ";
    fout<<SG3(graph,N)<<" ";
    fout<<EC(graph,N)<<" ";

    fout<<my_random_walk_cut.atrod_cut2(graph, N, N)<<" ";

    //fout<<pilna_parlase_Max_cut(graph,N)<<" ";

    fout<<endl;




    /*
    fout<<mans_max_cut_1var(graph, N)<<" ";
    fout<<mans_max_cut_2_2var(graph,N, 100)<<" ";
    fout<<mans_max_cut_3var(graph,N)<<" ";
    fout<<mans_Max_cut_random_v_parcelajs(graph,N)<<" ";
    fout<<random_Max_cut_1(graph, N, 500)<<" ";
    */

    /*
    fout<<mans_max_cut_2_2var(graph,N, 1)<<" ";
    fout<<random_Max_cut_1(graph, N, 1)<<" ";
    fout<<" |  ";
    */




# endif // TEST

    return 0;
}
