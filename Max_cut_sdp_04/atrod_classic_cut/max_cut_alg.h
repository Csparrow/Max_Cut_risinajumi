#ifndef MAX_CUT_ALG_H_INCLUDED
#define MAX_CUT_ALG_H_INCLUDED

using namespace std;

extern mt19937 my_rand;


struct edge{
    int to_vertice;
    int weight;
    edge(int v,int w){
        to_vertice=v;
        weight=w;
    }
    edge(int v){
        to_vertice=v;
        weight=1;
    }
    edge(){
        weight=0;
    }
};


struct vertice{
    vector<edge>edges;
    //int diff_w;
    bool cut_val;
};


///
///
int random_Max_cut_1(vertice *graph, const int vert_cnt, const int it_sk=1);

int mans_Max_cut_v_parcelajs(vertice *graph, const int vert_cnt);

int mans_Max_cut_random_v_parcelajs(vertice *graph, const int vert_cnt);

int mans_max_cut_1var(vertice *graph, const int vert_cnt);

int mans_max_cut_2var(vertice *graph, const int vert_cnt);

int mans_max_cut_2_2var(vertice *graph, const int vert_cnt, const int it_sk=10);

void DFS_liek_2dalas(vertice *graph, bool visited[], int cur_v, const bool cur_cut_val);

int mans_max_cut_3var(vertice *graph, const int vert_cnt);

int pilna_parlase_Max_cut(vertice *graph, const int vert_cnt);






#endif // MAX_CUT_ALG_H_INCLUDED
