#ifndef MAX_CUT_RANDOM_WALK_HPP_INCLUDED
#define MAX_CUT_RANDOM_WALK_HPP_INCLUDED




#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>
#include <ctime>
#include <cmath>

#include <random>

#include "max_cut_alg.h"


using namespace std;

/*
class max_cut_random_walk{
public:


    int izvelas_virs(const vector<pair<int,int>>&dw_edges){
        int a=0;
        int b=int(dw_edges.size())-1;
        int cur_ind=0;

        uniform_int_distribution<int>unifd(dw_edges[0].first, dw_edges[b].first);
        int el=unifd(my_rand);

        while(a<=b){
            cur_ind = (b-a)/2+a;
            if(el<dw_edges[cur_ind].first)b=cur_ind-1;
            else a=cur_ind+1;
        }


        return dw_edges[cur_ind].second;

    }//izvelas_virs



    void random_walk(int l, vector<vector<pair<int,int>>>&dw_edges, vector<int> &even_end_cnt, vector<int> &odd_end_cnt, int start_v){
        bernoulli_distribution bdist(0.5);
        int cur_v=start_v;
        int h=0;
        for(int a=0; a<l; ++a)if(bdist(my_rand))++h;

        for(int k=1; k<=h; ++k){
            cur_v=izvelas_virs(dw_edges[cur_v]);
            if(k&1)odd_end_cnt[cur_v]++;
            else even_end_cnt[cur_v]++;
        }
    }


    void threshold(vertice *graph, int vert_cnt, int start_v, int walk_cnt, int l, double t, char *klasif, vector<vector<pair<int,int>>>&dw_edges){
        vector<int>even_end_cnt(vert_cnt,0);
        vector<int>odd_end_cnt(vert_cnt,0);

        double diff,dj;

        for(int k=0; k<walk_cnt; ++k){
            random_walk(l,dw_edges, even_end_cnt, odd_end_cnt, start_v);
        }
//        for(int j=0; j<vert_cnt; ++j)printf("%3d ",even_end_cnt[j]);
//        printf("\n");
//        for(int j=0; j<vert_cnt; ++j)printf("%3d ",odd_end_cnt[j]);
//        printf("\n\n");


        for(int j=0; j<vert_cnt; ++j){
            if(klasif[j]=='*'){
                //nav vel klasificeta
                dj = (*(dw_edges[j].end()-1)).first;
                diff = double(even_end_cnt[j]-odd_end_cnt[j]);
                diff /= double(dj*walk_cnt);


                if(diff>t)klasif[j]='E';
                else if(diff<-t)klasif[j]='O';
            }
        }

    }//threshold





    int atrod_cut(vertice *graph, int vert_cnt){
        int rez=0;
        bernoulli_distribution bdist(0.5);

        double t=0.999901229;
        int l = (int)(round(log2(vert_cnt))+0.1);//l=logn
        int walk_cnt=(1000*l)/t;

        char *klasif=new char[vert_cnt];

        memset(klasif,'*',vert_cnt);


        vector<vector<pair<int,int>>>dw_edges(vert_cnt);

        for(int i=0,prev; i<vert_cnt; ++i){
            prev=0;
            for(edge e: graph[i].edges){
                prev+=e.weight;
                dw_edges[i].push_back(make_pair(prev,e.to_vertice));
            }
        }

        for(int i=0; i<vert_cnt; ++i){
            threshold(graph, vert_cnt, i, walk_cnt, l,t, klasif, dw_edges);
        }

        for(int i=0; i<vert_cnt; ++i)cout<<klasif[i]<<" ";
        cout<<endl;

        for(int i=0; i<vert_cnt; ++i){
            for(edge e: graph[i].edges){
                if(i<e.to_vertice && klasif[i]!='*'&& klasif[e.to_vertice]!='*' && klasif[i]!=klasif[e.to_vertice])rez+=e.weight;
            }
        }


        delete[]klasif;

        return rez;
    }//atrod_cut

};//end of class

*/


class max_cut_random_walk{
public:


    int izvelas_virs(const vector<pair<int,int>>&vert_dw_edges){
        int a=0;
        int b=int(vert_dw_edges.size())-1;
        int cur_ind=0;

        uniform_int_distribution<int>unifd(vert_dw_edges[0].first, vert_dw_edges[b].first);
        int el=unifd(my_rand);


        while(a<=b){
            cur_ind = (b-a)/2+a;
            if(el<vert_dw_edges[cur_ind].first)b=cur_ind-1;
            else a=cur_ind+1;
        }


        return vert_dw_edges[cur_ind].second;

    }//izvelas_virs



    void random_walk(int l, vector<vector<pair<int,int>>>&dw_edges, vector<int> &even_end_cnt, vector<int> &odd_end_cnt, int start_v){
        bernoulli_distribution bdist(0.5);
        int cur_v=start_v;
        int h=0;

        if(dw_edges[start_v].size()==0)return;

        for(int a=0; a<l; ++a)if(bdist(my_rand))++h;



        for(int k=1; k<=h; ++k){
            cur_v=izvelas_virs(dw_edges[cur_v]);

            if(k&1)odd_end_cnt[cur_v]++;
            else even_end_cnt[cur_v]++;
        }

    }




/*
    void threshold1(vertice *graph, int vert_cnt, int start_v, int l,int K, double t, char *klasif, vector<vector<pair<int,int>>>&dw_edges){
        vector<int>even_end_cnt(vert_cnt,0);
        vector<int>odd_end_cnt(vert_cnt,0);

        double diff,dj;

        int walk_cnt = int((K*log2(vert_cnt))/t);






        for(int k=0; k<walk_cnt; ++k){
            random_walk(l,dw_edges, even_end_cnt, odd_end_cnt, start_v);
        }
//        for(int j=0; j<vert_cnt; ++j)printf("%3d ",even_end_cnt[j]);
//        printf("\n");
//        for(int j=0; j<vert_cnt; ++j)printf("%3d ",odd_end_cnt[j]);
//        printf("\n\n");


        for(int j=0; j<vert_cnt; ++j){
            if(klasif[j]=='*'){
                //nav vel klasificeta
                dj = (*(dw_edges[j].end()-1)).first;
                diff = double(even_end_cnt[j]-odd_end_cnt[j]);
                diff /= double(dj*walk_cnt);

                //printf("%.5f  %.5f %.5f\n",diff,t,-t);
                //cin.get();


                if(diff>t)klasif[j]='E';
                else if(diff<-t)klasif[j]='O';
            }
        }

    }//threshold1





    int atrod_cut1(vertice *graph, int vert_cnt){


        int rez=0;
        bernoulli_distribution bdist(0.5);

        double t=1;
        int l = (int)(round(log2(vert_cnt))+0.1)+10;//l=logn
        int K=5;

        char *klasif=new char[vert_cnt];

        memset(klasif,'*',vert_cnt);

        int *Perm=new int[vert_cnt];

        for(int i=0; i<vert_cnt; ++i)Perm[i]=i;

        for(int i=0; i<vert_cnt; ++i){
            for(int j=0; j<vert_cnt; ++j){
                if(bdist(my_rand))swap(Perm[i],Perm[j]);
            }
        }


        vector<vector<pair<int,int>>>dw_edges(vert_cnt);

        for(int i=0,prev; i<vert_cnt; ++i){
            prev=0;
            for(edge e: graph[i].edges){
                prev+=e.weight;
                dw_edges[i].push_back(make_pair(prev,e.to_vertice));
            }
        }




        for(int i=0,v; i<vert_cnt; ++i){
            v = Perm[i];
            if(klasif[v]!='*')continue;

            t=1.0;
            while(t>0.0001){
                threshold1(graph, vert_cnt, v, l, K, t, klasif, dw_edges);
                t*=0.0009;
            }
        }

        for(int i=0; i<vert_cnt; ++i)cout<<klasif[i]<<" ";
        cout<<endl;

        for(int i=0; i<vert_cnt; ++i){
            for(edge e: graph[i].edges){
                if(i<e.to_vertice && klasif[i]!='*'&& klasif[e.to_vertice]!='*' && klasif[i]!=klasif[e.to_vertice])rez+=e.weight;
            }
        }


        delete[]klasif;
        delete[]Perm;

        return rez;
    }//atrod_cut

    */


void threshold2(int vert_cnt, int start_v, int l,int walk_cnt, char *klasif, vector<vector<pair<int,int>>>&dw_edges){
        vector<int>even_end_cnt(vert_cnt,0);
        vector<int>odd_end_cnt(vert_cnt,0);

        int diff;



        for(int k=0; k<walk_cnt; ++k){
            random_walk(l,dw_edges, even_end_cnt, odd_end_cnt, start_v);
        }
//        printf("\t%d\n",start_v);
//        for(int j=0; j<vert_cnt; ++j)printf("%3d ",even_end_cnt[j]);
//        printf("\n");
//        for(int j=0; j<vert_cnt; ++j)printf("%3d ",odd_end_cnt[j]);
//        printf("\n\n");


        for(int j=0; j<vert_cnt; ++j){
            if(klasif[j]=='*'){
                //nav vel klasificeta

                diff = even_end_cnt[j]-odd_end_cnt[j];
                if(diff>0)klasif[j]='E';
                else if(diff<0)klasif[j]='O';
            }
        }

    }//threshold2





    int atrod_cut2(vertice *graph, int vert_cnt, int walk_cnt){
        int rez=0;
        bernoulli_distribution bdist(0.5);

        int l = (int)(round(log2(vert_cnt))+0.1)+10;//l=logn


        char *klasif=new char[vert_cnt];

        memset(klasif,'*',vert_cnt);

        int *Perm=new int[vert_cnt];

        for(int i=0; i<vert_cnt; ++i)Perm[i]=i;

        for(int i=0; i<vert_cnt; ++i){
            for(int j=0; j<vert_cnt; ++j){
                if(bdist(my_rand))swap(Perm[i],Perm[j]);
            }
        }



        vector<vector<pair<int,int>>>dw_edges(vert_cnt);

        for(int i=0,prev; i<vert_cnt; ++i){
            prev=0;
            for(edge e: graph[i].edges){
                prev+=e.weight;
                dw_edges[i].push_back(make_pair(prev,e.to_vertice));
            }
        }




        for(int i=0,v; i<vert_cnt; ++i){
            v = Perm[i];
            if(klasif[v]!='*')continue;
            threshold2(vert_cnt, v, l, walk_cnt, klasif, dw_edges);
        }

//        for(int i=0; i<vert_cnt; ++i)cout<<klasif[i]<<" ";
//        cout<<endl;


        for(int i=0; i<vert_cnt; ++i){
            if(klasif[i]=='*'){
                if(bdist(my_rand))klasif[i]='E';
                else klasif[i]='O';
            }
        }



        for(int i=0; i<vert_cnt; ++i){
            if(klasif[i]=='E')graph[i].cut_val=true;
            else graph[i].cut_val=false;

            for(edge e: graph[i].edges){
                if(i<e.to_vertice && klasif[i]!=klasif[e.to_vertice])rez+=e.weight;
            }
        }



        delete[]klasif;
        delete[]Perm;

        return rez;
    }//atrod_cut1


};//end of class






#endif // MAX_CUT_RANDOM_WALK_HPP_INCLUDED
