#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>

#include <random>

using namespace std;



mt19937 my_rand;


struct edge{
    int to_vertice;
    double weight;
    edge(int v,double w){
        to_vertice=v;
        weight=w;
    }
    edge(){}
};


struct vertice{
    vector<edge>edges;
    double diff_w;
    bool cut_val;
    vertice(){
        diff_w=0;
    }
};




double random_Max_cut_1(vertice *graph, const int vert_cnt){

    double rez=0;
    //mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    edge e;

    //my_rand.seed(rand());


    for(int i=0; i<vert_cnt; ++i){
        //cut[i]=my_dist(my_rand);
        //printf("%d ",cut[i]);
        graph[i].cut_val = my_dist(my_rand);
    }
    //printf("  ");


    for(int i=0; i<vert_cnt; ++i){
        for(int j=0; j<graph[i].edges.size(); ++j){
            e=graph[i].edges[j];
            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez+=e.weight;
            }
        }
    }
    //printf("cur rez:%.2f\n",rez);



    return rez;

}//random_Max_cut_1


//
//double mans_Max_cut_1(vertice *graph, const int vert_cnt){
//
//
//    double rez=0;
//
//    double best_diff;
//    double cur_diff;
//    int *best_v=new int[vert_cnt];
//    int len;
//
//    int chosen_vert_ind;
//
//    uniform_int_distribution<int> my_dist;
//
//    for(int i=0; i<vert_cnt; ++i)graph[i].cut_val=0;
//
//
//    while(true){
//        best_diff=0;
//        len=0;
//        /atrod 'max'
//        for(int i=0; i<vert_cnt; ++i){
//            if(graph[i].cut_val) continue;
//            cur_diff=graph[i].diff_w;
//            if(cur_diff>best_diff){
//                len=1;
//                best_v[0]=i;
//                best_diff=cur_diff;
//            }
//            else if(cur_diff==best_diff)best_v[len++]=i;
//        }
//
//        if(best_diff>0){
//            if(len==1)chosen_vert_ind = best_v[0];
//            else chosen_vert_ind = best_v[my_dist(my_rand)%len];
//
//
//            for(edge e: graph[chosen_vert_ind].edges){
//                if(!graph[e.to_vertice].cut_val){
//                    graph[e.to_vertice].diff_w-=e.weight;
//                }
//            }
//            graph[chosen_vert_ind].cut_val=1;
//        }
//        else break;
//
//    }
//
//
//
//    for(int i=0; i<vert_cnt; ++i){
//        for(edge e: graph[i].edges){
//            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
//                rez+=e.weight;
//            }
//        }
//    }
//
//
//
//    delete []best_v;
//
//    return rez;
//}//mans_Max_cut_1



double mans_Max_cut_2(vertice *graph, const int vert_cnt){
    double rez=0;


    double cur_diff, best_diff;
    int *best_v=new int[vert_cnt];
    int len;

    int chosen_vert_ind;
    bool cut_side;
    bool ok;

    uniform_int_distribution<int> my_dist;


    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=0;
        graph[i].diff_w=0;
        for(edge e: graph[i].edges){
            graph[i].diff_w+=e.weight;
        }
    }




    do{
        ok=false;

        best_diff=0;
        len=0;
        cut_side=0;
        ///atrod 'max'
        for(int i=0; i<vert_cnt; ++i){
            if(graph[i].cut_val!=cut_side) continue;
            cur_diff=graph[i].diff_w;
            if(cur_diff>best_diff){
                len=1;
                best_v[0]=i;
                best_diff=cur_diff;
            }
            else if(cur_diff==best_diff)best_v[len++]=i;
        }


        if(best_diff>0){
            ok=true;
            if(len==1)chosen_vert_ind = best_v[0];
            else chosen_vert_ind = best_v[my_dist(my_rand)%len];


            for(edge e: graph[chosen_vert_ind].edges){
                if(graph[e.to_vertice].cut_val==cut_side){
                    graph[e.to_vertice].diff_w-=e.weight;
                    graph[chosen_vert_ind].diff_w-=e.weight;
                }
                else{
                    graph[e.to_vertice].diff_w+=e.weight;
                    graph[chosen_vert_ind].diff_w+=e.weight;
                }
            }
            graph[chosen_vert_ind].cut_val=!cut_side;

        }

        best_diff=0;
        len=0;
        cut_side=1;
        ///atrod 'max'
        for(int i=0; i<vert_cnt; ++i){
            if(graph[i].cut_val!=cut_side) continue;
            cur_diff=graph[i].diff_w;

            if(cur_diff>best_diff){
                len=1;
                best_v[0]=i;
                best_diff=cur_diff;
            }
            else if(cur_diff==best_diff)best_v[len++]=i;
        }


        if(best_diff>0){
            ok=true;
            if(len==1)chosen_vert_ind = best_v[0];
            else chosen_vert_ind = best_v[my_dist(my_rand)%len];

            for(edge e: graph[chosen_vert_ind].edges){
                if(graph[e.to_vertice].cut_val==cut_side){
                    graph[e.to_vertice].diff_w-=e.weight;
                    graph[chosen_vert_ind].diff_w-=e.weight;
                }
                else{
                    graph[e.to_vertice].diff_w+=e.weight;
                    graph[chosen_vert_ind].diff_w+=e.weight;
                }
            }
            graph[chosen_vert_ind].cut_val=!cut_side;
        }
        cin.get();

    }
    while(ok);










    for(int i=0; i<vert_cnt; ++i){
        for(edge e: graph[i].edges){
            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez+=e.weight;
            }
        }
    }



    delete []best_v;

    return rez;
}//mans_Max_cut_2





double pilna_parlase_Max_cut(vertice *graph, const int vert_cnt){

    double best_rez=0;
    double cur_rez;

    if(vert_cnt>25){
        printf("\n\nVirsotnu skaits:%d par lielu, lai veiktu pilno parlasi!!\n");
        return -1;
    }

    int var_cnt=(1<<vert_cnt);

    for(int cur_var=0, temp; cur_var<var_cnt; ++cur_var){
        temp=cur_var;
        cur_rez=0;
        for(int i=vert_cnt-1; i>=0; --i){
            graph[i].cut_val = (bool)(temp&1);
            temp>>=1;
        }

        for(int i=0; i<vert_cnt; ++i){
            for(edge e: graph[i].edges){
                if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                    cur_rez+=e.weight;
                }
            }
        }
        if(cur_rez>best_rez)best_rez=cur_rez;
    }

    return best_rez;
}//pilna_parlase_Max_cut



int main()
{
    vertice *graph;
    int vert_cnt;
    fstream fin("in.txt",ios::in);
    int u,v;
    double cur_w;

    double tmp_rez, rez;

    fin>>vert_cnt;
    ++vert_cnt;



    graph=new vertice[vert_cnt];

    while(fin>>u>>v>>cur_w){
        graph[u].edges.push_back(edge(v,cur_w));
        graph[u].diff_w+=cur_w;

        graph[v].edges.push_back(edge(u,cur_w));
        graph[v].diff_w+=cur_w;
    }

    for(int i=0; i<vert_cnt; ++i){
        //printf("%d: ",i);
        for(int j=0; j<graph[i].edges.size(); ++j){
            //printf("\t%d  %.2f\n",graph[i].edges[j].to_vertice, graph[i].edges[j].weight);
        }
        //printf("\n");
    }



    int N=2;
//
//    tmp_rez=0;
//    rez=0;
//    my_rand.seed(my_rand());
//    for(int i=0; i<N; ++i){
//        tmp_rez=random_Max_cut_1(graph, vert_cnt);
//        if(tmp_rez>rez)rez=tmp_rez;
//        if(tmp_rez>=10.49){
//            for(int i=0; i<vert_cnt; ++i)printf("%d %d\n",i, graph[i].cut_val);
//        }
//    }
//    printf("\nrandom var rez=%.2f\n",rez);


//    tmp_rez=0;
//    rez=0;
//    for(int i=0; i<N; ++i){
//        tmp_rez=mans_Max_cut_1(graph, vert_cnt);
//        if(tmp_rez>rez)rez=tmp_rez;
//        if(tmp_rez>=10.49){
//            for(int i=0; i<vert_cnt; ++i)printf("%d %d\n",i, graph[i].cut_val);
//        }
//    }
//    printf("\njaunais var rez=%.2f\n",rez);

    tmp_rez=0;
    rez=0;
    for(int i=0; i<N; ++i){
        tmp_rez=mans_Max_cut_2(graph, vert_cnt);
        if(tmp_rez>rez)rez=tmp_rez;
        if(tmp_rez>=10.49){
            for(int i=0; i<vert_cnt; ++i)printf("%d %d\n",i, graph[i].cut_val);
        }
    }
    printf("\njaunais 2.var rez=%.2f\n",rez);



//    rez=pilna_parlase_Max_cut(graph, vert_cnt);
//    printf("\npilnas parlases rez: %.2f\n",rez);



    return 0;
}//main
