#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>

#include <random>

using namespace std;



mt19937 my_rand;


struct edge{
    int to_vertice;
    int weight;
    edge(int v,int w){
        to_vertice=v;
        weight=w;
    }
    edge(int v){
        to_vertice=v;
        weight=0;
    }
    edge(){
        weight=0;
    }
};


struct vertice{
    vector<edge>edges;
    //int diff_w;
    bool cut_val;
};




int random_Max_cut_1(vertice *graph, const int vert_cnt){

    int rez=0;
    //mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);

    edge e;

    //my_rand.seed(rand());


    for(int i=0; i<vert_cnt; ++i){
        //cut[i]=my_dist(my_rand);
        //printf("%d ",cut[i]);
        graph[i].cut_val = my_dist(my_rand);
    }
    //printf("  ");


    for(int i=0; i<vert_cnt; ++i){
        for(int j=0; j<graph[i].edges.size(); ++j){
            e=graph[i].edges[j];
            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez+=e.weight;
            }
        }
    }
    //printf("cur rez:%.2f\n",rez);



    return rez;

}//random_Max_cut_1





int mans_Max_cut_2(vertice *graph, const int vert_cnt){
    int rez=0;


    int cur_diff, best_diff;
    pair<int,bool>*best_v=new pair<int,bool>[vert_cnt];
    int *w0=new int[vert_cnt];
    int *w1=new int[vert_cnt];
    int *p_no,*p_uz;
    int len;

    int chosen_vert_ind;
    bool cur_cut_side;

    uniform_int_distribution<int> my_dist;


    for(int i=0; i<vert_cnt; ++i){
        graph[i].cut_val=0;
        w0[i]=0;
        w1[i]=0;
        for(edge e: graph[i].edges){
            w0[i]+=e.weight;
        }
    }




    while(true){

        best_diff=0;
        len=0;
        ///atrod 'max'
        for(int i=0; i<vert_cnt; ++i){
            cur_cut_side=graph[i].cut_val;
            if(cur_cut_side) cur_diff=w1[i]-w0[i];
            else cur_diff=w0[i]-w1[i];

            if(cur_diff>best_diff){
                len=1;
                best_v[0]=pair<int,bool>(i,cur_cut_side);
                best_diff=cur_diff;
            }
            else if(cur_diff==best_diff)best_v[len++]=pair<int,bool>(i,cur_cut_side);
        }



        if(best_diff>0){
            if(len==1){
                chosen_vert_ind = best_v[0].first;
                cur_cut_side = best_v[0].second;
            }
            else{
                int temp_ind=my_dist(my_rand)%len;
                chosen_vert_ind = best_v[temp_ind].first;
                cur_cut_side = best_v[temp_ind].second;
            }

            p_no=w0;
            p_uz=w1;
            if(cur_cut_side)swap(p_no, p_uz);



            for(edge e: graph[chosen_vert_ind].edges){
                p_no[e.to_vertice]-=e.weight;
                p_uz[e.to_vertice]+=e.weight;
                    /*
                if(graph[e.to_vertice].cut_val==cur_cut_side){
                    p_no[e.to_vertice]-=e.weight;
                    p_uz[e.to_vertice]+=e.weight;
                }
                else{
                    p_no[e.to_vertice]+=e.weight;
                    p_uz[e.to_vertice]-=e.weight;
                }
                */
            }
            graph[chosen_vert_ind].cut_val=!cur_cut_side;

        }
        else break;


    }




    for(int i=0; i<vert_cnt; ++i){
        for(edge e: graph[i].edges){
            if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                rez+=e.weight;
            }
        }
    }



    delete []best_v;
    delete []w0;
    delete []w1;

    return rez;
}//mans_Max_cut_2





int pilna_parlase_Max_cut(vertice *graph, const int vert_cnt){

    int best_rez=0;
    int cur_rez;

    if(vert_cnt>25){
        printf("\n\nVirsotnu skaits:%d par lielu, lai veiktu pilno parlasi!!\n");
        return -1;
    }

    int var_cnt=(1<<vert_cnt);

    for(int cur_var=0, temp; cur_var<var_cnt; ++cur_var){
        temp=cur_var;
        cur_rez=0;
        for(int i=vert_cnt-1; i>=0; --i){
            graph[i].cut_val = (bool)(temp&1);
            temp>>=1;
        }

        for(int i=0; i<vert_cnt; ++i){
            for(edge e: graph[i].edges){
                if(e.to_vertice>i && graph[i].cut_val!=graph[e.to_vertice].cut_val){
                    cur_rez+=e.weight;
                }
            }
        }
        if(cur_rez>best_rez)best_rez=cur_rez;
    }

    return best_rez;
}//pilna_parlase_Max_cut



int main()
{
    vertice *graph;
    int vert_cnt;
    fstream fin("in2.txt",ios::in);
    int u,v;
    int cur_w;

    int tmp_rez, rez;

    fin>>vert_cnt;
    ++vert_cnt;



    graph=new vertice[vert_cnt];

    while(fin>>u>>v>>cur_w){
        graph[u].edges.push_back(edge(v,cur_w));
        //graph[u].diff_w+=cur_w;

        graph[v].edges.push_back(edge(u,cur_w));
        //graph[v].diff_w+=cur_w;
    }

    for(int i=0; i<vert_cnt; ++i){
        //printf("%d: ",i);
        for(int j=0; j<graph[i].edges.size(); ++j){
            //printf("\t%d  %d\n",graph[i].edges[j].to_vertice, graph[i].edges[j].weight);
        }
        //printf("\n");
    }



    int N=1000;
    tmp_rez=0;
    rez=0;
    my_rand.seed(my_rand());
    for(int i=0; i<N; ++i){
        tmp_rez=random_Max_cut_1(graph, vert_cnt);
        if(tmp_rez>rez)rez=tmp_rez;

    }
    printf("\nrandom var rez=%d\n",rez);





    rez=mans_Max_cut_2(graph, vert_cnt);
    printf("\njaunais 2.var rez=%d\n",rez);



//    rez=pilna_parlase_Max_cut(graph, vert_cnt);
//    printf("\npilnas parlases rez: %d\n",rez);



    return 0;
}//main
