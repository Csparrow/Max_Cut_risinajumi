#include <iostream>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <vector>

#include <random>

using namespace std;



mt19937 my_rand;


struct edge{
    int to_vertice;
    double weight;
    edge(int v,double w){
        to_vertice=v;
        weight=w;
    }
    edge(){}
};


struct vertice{
    vector<edge>edges;
    double diff_w;
    vertice(){
        diff_w=0;
    }
};




double random_Max_cut_1(const vertice *graph, const int vert_cnt){

    double rez=0;
    //mt19937 my_rand;
    bernoulli_distribution my_dist(0.5);
    bool *cut=new bool[vert_cnt];

    edge e;

    //my_rand.seed(rand());

    //printf("\n");
    for(int i=0; i<vert_cnt; ++i){
        cut[i]=my_dist(my_rand);
        //printf("%d ",cut[i]);
    }
    //printf("  ");


    for(int i=0; i<vert_cnt; ++i){
        for(int j=0; j<graph[i].edges.size(); ++j){
            e=graph[i].edges[j];
            if(e.to_vertice>i && cut[i]!=cut[e.to_vertice]){
                rez+=e.weight;
            }
        }
    }
    //printf("cur rez:%.2f\n",rez);


    delete []cut;

    return rez;

}//random_Max_cut_1



double mans_Max_cut_1(vertice *graph, const int vert_cnt){
    double rez=0;
    bool *cut=new bool[vert_cnt];

    double best_diff;
    double cur_diff;
    int *best_v=new int[vert_cnt];
    int len;

    int chosen_vert_ind;

    uniform_int_distribution<int> my_dist;

    memset(cut, 0, vert_cnt);


    while(true){
        best_diff=0;
        len=0;
        ///atrod 'max'
        for(int i=0; i<vert_cnt; ++i){
            if(cut[i]) continue;
            cur_diff=graph[i].diff_w;
            if(cur_diff>best_diff){
                len=1;
                best_v[0]=i;
                best_diff=cur_diff;
            }
            else if(cur_diff==best_diff)best_v[len++]=i;
        }

        if(best_diff>0){
            if(len==1)chosen_vert_ind = best_v[0];
            else chosen_vert_ind = best_v[my_dist(my_rand)%len];


            for(edge e: graph[chosen_vert_ind].edges){
                if(!cut[e.to_vertice]){
                    graph[e.to_vertice].diff_w-=e.weight;
                }
            }
            cut[chosen_vert_ind]=1;
        }
        else break;

    }



    for(int i=0; i<vert_cnt; ++i){
        for(edge e: graph[i].edges){
            if(e.to_vertice>i && cut[i]!=cut[e.to_vertice]){
                rez+=e.weight;
            }
        }
    }



    delete []best_v;
    delete []cut;

    return rez;
}//mans_Max_cut_1



int main()
{
    vertice *graph;
    int vert_cnt;
    fstream fin("in.txt",ios::in);
    int u,v;
    double cur_w;

    double tmp_rez, rez;

    fin>>vert_cnt;
    ++vert_cnt;



    graph=new vertice[vert_cnt];

    while(fin>>u>>v>>cur_w){
        graph[u].edges.push_back(edge(v,cur_w));
        graph[u].diff_w+=cur_w;

        graph[v].edges.push_back(edge(u,cur_w));
        graph[v].diff_w+=cur_w;
    }

    for(int i=0; i<vert_cnt; ++i){
        //printf("%d: ",i);
        for(int j=0; j<graph[i].edges.size(); ++j){
            //printf("\t%d  %.2f\n",graph[i].edges[j].to_vertice, graph[i].edges[j].weight);
        }
        //printf("\n");
    }



    int N=100000;

    tmp_rez=0;
    rez=0;
    for(int i=0; i<N; ++i){
        tmp_rez=random_Max_cut_1(graph, vert_cnt);
        if(tmp_rez>rez)rez=tmp_rez;
    }
    printf("\nrandom var rez=%.2f\n",rez);


//    tmp_rez=0;
//    rez=0;
//    for(int i=0; i<N; ++i){
//        tmp_rez=mans_Max_cut_1(graph, vert_cnt);
//        if(tmp_rez>rez)rez=tmp_rez;
//    }
//    printf("\njaunais var rez=%.2f\n",rez);




    return 0;
}//main
