#include <iostream>
#include <cstdio>
#include <random>
#include <fstream>
#include <cstring>

#include <Eigen/Dense>
#include <Eigen/Cholesky>


using namespace std;
int main()
{
  mt19937 my_rand;
  uniform_real_distribution<double> my_dist(-1,1);


  fstream fin;
  int N;
  double a;
  int **W;
  int v1,v2,w;

  int cur_cut=0;
  int best_cut=0;



  fin.open("graph.txt",ios::in);
  fin>>N;
  Eigen::MatrixXd Y(N,N);

  W=new int*[N];
  for(int i=0; i<N; ++i){
    W[i]=new int[N];
    memset(W[i],0,4*N);
  }

  while(fin>>v1>>v2>>w){
    W[v1][v2]=w;
    W[v2][v1]=w;
  }
  fin.close();


  fin.open("Y_matrica.txt",ios::in);
  for(int i=0; i<N; ++i){
    for(int j=0; j<N;++j){
        fin>>a;
        Y(i,j)=a;
    }
  }
  fin.close();

  Eigen::VectorXd r(N);
  Eigen::VectorXd s(N);

  Eigen::MatrixXd L( Y.llt().matrixL() );

  for(int i=0; i<N; ++i)r(i)=my_dist(my_rand);
  r.normalize();


  for(int i=0; i<N; ++i){
    if(r.dot(L.row(i))>=0){
        s(i)=1;
    }
    else{
        s(i)=-1;
    }
  }


  ///
  ///


  cur_cut=0;
  for(int i=0; i<N; ++i){
    for(int j=i+1; j<N; ++j){
        if(s[i]!=s[j])cur_cut+=W[i][j];
    }
  }
  printf("cur cut:=%d\n", cur_cut);







  /*
  Eigen::MatrixXd m(2,2);
  m(0,0) = 1;
  m(1,0) = -1;
  m(0,1) = 1;
  m(1,1) = -1;
  cout << m << endl;

  Eigen::MatrixXd L( m.llt().matrixL() );
  cout << L << std::endl;
  */
}
