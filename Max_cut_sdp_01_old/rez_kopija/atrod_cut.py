import numpy as np
import random



with open("Y_matrica.txt","r") as f:
    N=int(f.readline())
    Y=[[0.0 for i in range(N)] for j in range(N)]
    temp=f.read()
    temp=temp.replace("{","")
    temp=temp.replace("}", "")
    temp=temp.replace(",", " ")
    s=temp.split()
    for i in range(N):
        for j in range(N):
            Y[i][j]=float(s[i*N+j])


L = np.linalg.cholesky(Y)
LT = L.transpose()

exit(-1)


r=np.zeros((N,))
s=np.zeros((N,))

rez_cut=0
with open("graph.txt", "r") as f:
    f.readline()
    W = np.zeros((N, N))
    for line in f:
        if (line[0] == "\n"): continue
        a, b, w = [int(x) for x in line.split()]
        W[a][b] = w
        W[b][a] = w




with open("cut_rez.txt","a") as f:
    print("N =",N, file=f)
    for q in range(10):
        for i in range(N):
            r[i] = random.uniform(-1, 1)
        norm = np.linalg.norm(r)
        for i in range(N):
            r[i] = r[i] / norm

        for i in range(N):
            if (np.dot(r, LT[i]) >= 0):
                s[i] = 1
            else:
                s[i] = -1

        cur_cut = 0
        for i in range(N):
            for j in range(i + 1, N):
                if (s[i] != s[j]): cur_cut += W[i][j]
        print(cur_cut, file=f)
        rez_cut=max(cur_cut,rez_cut)

    print("rez cut:=",rez_cut, file=f)
    print("\n##############################\n",file=f)

