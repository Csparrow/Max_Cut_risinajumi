#include <iostream>
#include <cstdio>
#include <cstring>



using namespace std;

int main()
{
    double W[100][100];
    FILE *file_in=fopen("graph.txt", "r");
    FILE *file_out=fopen("my_sdpa_input.dat", "w");

    int vert_cnt;
    int a,b,w;
    double sum;

    for(int i=0; i<vert_cnt; ++i){
        for(int j=0; j<vert_cnt; ++j)W[i][j]=0.0;
    }

    fscanf(file_in,"%d",&vert_cnt);

    while(fscanf(file_in,"%d %d %d",&a, &b, &w)==3){
        W[a][b]=w;
        W[b][a]=w;
    }


    for(int i=0; i<vert_cnt; ++i){
        sum=0;
        for(int j=0; j<vert_cnt; ++j){
            sum+=W[i][j];
            W[i][j]*=-0.25;
        }
        W[i][i]=sum*0.25 + W[i][i];
    }

    fprintf(file_out,"%d = mDIM\n1 = nBLOCK\n%d = bLOCKsTRUCT\n\n", vert_cnt, vert_cnt);

    for(int i=0; i<vert_cnt; ++i)fprintf(file_out,"1 ");
    fprintf(file_out,"\n\n");



    //F0
    fprintf(file_out,"{");
    for(int i=0; i<vert_cnt; ++i){
        fprintf(file_out,"{");
        for(int j=0; j<vert_cnt; ++j){
            fprintf(file_out,"%f ", W[i][j]);
        }
        fprintf(file_out,"} ");
    }
    fprintf(file_out,"}\n\n");




    for(int i=0; i<vert_cnt; ++i){
        for(int j=0; j<vert_cnt; ++j)W[i][j]=0.0;
    }



    for(int k=0; k<vert_cnt; ++k){
        W[k][k]=1;

        fprintf(file_out,"{");
        for(int i=0; i<vert_cnt; ++i){
            fprintf(file_out,"{");
            for(int j=0; j<vert_cnt; ++j){
                fprintf(file_out,"%f ", W[i][j]);
            }
            fprintf(file_out,"} ");
        }
        fprintf(file_out,"}\n\n");

        W[k][k]=0;
    }




    return 0;
}
