#include <iostream>
#include <cstdio>
#include <random>
#include <fstream>
#include <cstring>

#include <Eigen/Dense>
#include <Eigen/Cholesky>

#include "max_cut_alg.h"
#include "max_cut_alg.cpp"


using namespace std;

uniform_real_distribution<double> my_dist2(-1,1);


int atrod_sdp_cut(vertice *graph,const int N, const Eigen::MatrixXd &L){
    int rez=0;

    Eigen::VectorXd r(N);
    //Eigen::VectorXd s(N);
    for(int i=0; i<N; ++i)r(i)=my_dist2(my_rand);
    r.normalize();


    for(int i=0; i<N; ++i){
        if(r.dot(L.row(i))>=0){
            //s(i)=1;
            graph[i].cut_val=0;
        }
        else{
            //s(i)=-1;
            graph[i].cut_val=1;
        }
    }

    for(int i=0; i<N; ++i){
        for(edge e: graph[i].edges){
            if(i<e.to_vertice && graph[i].cut_val!=graph[e.to_vertice].cut_val)rez+=e.weight;
        }
    }

    return rez;
}//atrod_sdp_cut


int main()
{

  fstream fin;
  int N;
  double a;
  vertice *graph=new vertice[1000];
  int v1,v2,w;


  int cur_cut;
  int min_sdp_cut;
  int max_sdp_cut;


  fin.open("graph.txt",ios::in);
  fin>>N;
  Eigen::MatrixXd Y(N,N);

  FILE *f=fopen("cut_rez.txt","a");



  while(fin>>v1>>v2>>w){
    graph[v1].edges.push_back(edge(v2,w));
    graph[v2].edges.push_back(edge(v1,w));
  }
  fin.close();


  fin.open("Y_matrica.txt",ios::in);
  for(int i=0; i<N; ++i){
    for(int j=0; j<N;++j){
        fin>>a;
        Y(i,j)=a;
    }
  }
  fin.close();


  //Eigen::MatrixXd L( Y.llt().matrixL() );


  //Eigen::MatrixXd L(Y.llt().matrixLDLT());

  Eigen::LDLT<Eigen::MatrixXd>qw(Y);


  Eigen::MatrixXd L=Y.ldlt().matrixL();

  Eigen::MatrixXd A(3,3);
  A<<4,12,-16,12,37,-43,-16,-43,98;

  Eigen::MatrixXd L2=(A.ldlt().matrixL())*(A.ldlt().matrixU());
  cout<<L2<<endl<<endl;
  return -1;




  fprintf(f,"N = %d\n",N);
  max_sdp_cut=atrod_sdp_cut(graph,N,L);
  min_sdp_cut=max_sdp_cut;

  for(int qw=0; qw<19; ++qw){
      cur_cut=atrod_sdp_cut(graph,N,L);
      max_sdp_cut=max(max_sdp_cut,cur_cut);
      min_sdp_cut=min(min_sdp_cut,cur_cut);
  }
  fprintf(f,"%d ", mans_max_cut_1var(graph, N));
  fprintf(f,"%d ",mans_max_cut_2_2var(graph,N, 100));
  fprintf(f,"%d ", mans_max_cut_3var(graph,N));
  fprintf(f,"%d ", mans_Max_cut_random_v_parcelajs(graph,N));
  fprintf(f,"%d  | ", random_Max_cut_1(graph, N, 500));
  fprintf(f,"%d  %d\n",min_sdp_cut,max_sdp_cut);

  fprintf(f,"##########\n\n");



}//main
